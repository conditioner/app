# Contributing

Thanks for the help!

## :fork_and_knife: Fork The Repo

To get started and have a copy of the code to work on fork the repo.

Create branches from `main` in your fork and add your changes there.

## :white_check_mark: Testing Your Changes

Make sure all current tests pass with your changes.

Add your own tests that cover the changes you've made.

## :books: Updating Documentation

Update or add any markdown files for high level documentation of your changes if
it makes sense.

Add comments to code that is meaningful when a bit of logic or purpose is not
naturally clear.

## :inbox_tray: Submitting Your Code

Create merge requests with a clear title and description of the change.

Include if applicable:

* Recommended test scenarios and test steps for each
* Any remaining work (e.g. further changes that may come in a future MR)
* Additional notes of interest
* Any bugs/issues that the change fixes or relates to
* Any feature requests the the change resolves or relates to

In general add as much detail as you can to paint a clear picture of what you're
submitting.
