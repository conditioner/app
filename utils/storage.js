import AsyncStorage from '@react-native-async-storage/async-storage'
import isMatch from 'lodash.ismatch'

const saveItem = async (collection, item) => {
  const currentData = await AsyncStorage.getItem(collection)
  const items = currentData ? JSON.parse(currentData) : []
  const itemIndex = items.findIndex(i => i.id === item.id)

  if (itemIndex >= 0) {
    items[itemIndex] = item
  } else {
    items.push(item)
  }

  await AsyncStorage.setItem(collection, JSON.stringify(items))
}

const getItem = async (collection, id) => {
  const currentData = await AsyncStorage.getItem(collection)
  const items = currentData ? JSON.parse(currentData) : []
  const itemIndex = items.findIndex(i => i.id === id)

  return itemIndex >= 0 ? items[itemIndex] : null
}

const getItems = async (collection, query = {}) => {
  const currentData = await AsyncStorage.getItem(collection)
  if (currentData) {
    return JSON.parse(currentData).filter(d => isMatch(d, query))
  } else {
    return []
  }
}

const deleteItems = async (collection, ids = []) => {
  if (ids.length === 0) {
    return
  }
  const currentData = await AsyncStorage.getItem(collection)

  if (currentData) {
    const items = JSON.parse(currentData)
    const updated = items.filter(i => !ids.includes(i.id))

    await AsyncStorage.setItem(collection, JSON.stringify(updated))
  }
}

export {
  saveItem,
  getItem,
  getItems,
  deleteItems
}
