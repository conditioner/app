import { getOutput } from './api'
import get from 'lodash.get'

const conditions = {
  EQUALS: {
    stringValue: 'equals',
    symbol: '=',
    evaluate: (a, b) => a === b
  },
  GREATER_THAN: {
    stringValue: 'is greater than',
    symbol: '>',
    evaluate: (a, b) => a > b
  },
  LESS_THAN: {
    stringValue: 'is less than',
    symbol: '<',
    evaluate: (a, b) => a < b
  }
}

/**
 * Given an API call, operator and value will evaluate a condition returning
 * true or false.
 * @param {Object} apiCall
 * @param {String} operator
 * @param {String} value
 */
const checkCondition = async (apiCall, operator, value, returnApiResult = false) => {
  const apiResult = await getOutput(apiCall)
  const compareValue = isNaN(value) ? value : parseInt(value)

  const evaluate = get(conditions, `${operator}.evaluate`)
  if (evaluate) {
    if (returnApiResult) {
      return {
        conditionResult: evaluate(apiResult, compareValue),
        apiResult
      }
    }
    return evaluate(apiResult, compareValue)
  }
}

const getOperatorSymbol = (operator) => {
  return get(conditions, `${operator}.stringValue`, '')
}

export {
  conditions,
  checkCondition,
  getOperatorSymbol
}
