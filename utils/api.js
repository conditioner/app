import axios from 'axios'
import get from 'lodash.get'

/**
 * Given an API call record will attempt to send a API request.
 * @param {Object} apiCall
 */
const sendRequest = (apiCall) => {
  const { url } = apiCall

  return axios.get(url)
    .then(response => response.data)
}

/**
 * Given an API call record will attempt to send a API request and return the
 * specific data for the dataPath.
 * @param {Object} apiCall
 */
const getOutput = (apiCall) => {
  return sendRequest(apiCall)
    .then(data => apiCall.dataPath ? get(data, apiCall.dataPath) : data)
}

export {
  sendRequest,
  getOutput
}
