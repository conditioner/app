import { checkCondition } from './condition'
import { getItem } from './storage'

const execute = async (node) => {
  if (node.type === 'output') {
    return node.value || ''
  } else if (node.type === 'if') {
    const { condition, trueBranch, falseBranch } = node

    if (condition && (trueBranch || falseBranch)) {
      const { apiId, operator, value } = await getItem('conditions', node.condition)
      const apiCall = await getItem('api_calls', apiId)
      const conditionResult = await checkCondition(apiCall, operator, value)

      if (conditionResult && trueBranch) {
        const nextNode = await getItem('nodes', node.trueBranch)
        return execute(nextNode)
      } else if (!conditionResult && falseBranch) {
        const nextNode = await getItem('nodes', node.falseBranch)
        return execute(nextNode)
      }
    }
    // trueBranch or falseBranch missing default to '' output
    return ''
  } else {
    return 'Error: invalid node'
  }
}

const getRootNode = async (node) => {
  if (Object.prototype.hasOwnProperty.call(node, 'parent')) {
    const parent = await getItem('nodes', node.parent)
    return getRootNode(parent)
  }
  return node
}

const getChildren = async (node) => {
  if (!node || node.type === 'output') {
    return []
  }
  const children = []
  if (node.type === 'if') {
    const { trueBranch, falseBranch } = node
    if (trueBranch) {
      children.push(node.trueBranch)
      const childNode = await getItem('nodes', node.trueBranch)
      const childNodeChildren = await getChildren(childNode)
      children.push(...childNodeChildren)
    }
    if (falseBranch) {
      children.push(node.falseBranch)
      const childNode = await getItem('nodes', node.falseBranch)
      const childNodeChildren = await getChildren(childNode)
      children.push(...childNodeChildren)
    }
  }

  return children
}

export {
  execute,
  getRootNode,
  getChildren
}
