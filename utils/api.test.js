import axios from 'axios'
import { sendRequest, getOutput } from './api'

jest.mock('axios')

describe('api', () => {
  describe('sendRequest', () => {
    it('should send a API request for the passed record', async () => {
      const apiCall = { url: 'https://foo.bar' }
      axios.get.mockResolvedValue({})

      await sendRequest(apiCall)

      expect(axios.get).toHaveBeenLastCalledWith('https://foo.bar')
    })
    it('should resolve with the response body of a API call', async () => {
      const apiCall = { url: 'https://foo.bar' }
      axios.get.mockResolvedValue({ data: { foo: 'bar' } })

      const responseData = await sendRequest(apiCall)

      expect(responseData).toEqual({ foo: 'bar' })
    })
  })

  describe('getOutput', () => {
    it('should resolve with the response body data path of a API call if set', async () => {
      const apiCall = { url: 'https://foo.bar', dataPath: 'foo' }
      axios.get.mockResolvedValue({ data: { foo: 'bar' } })

      const responseData = await getOutput(apiCall)

      expect(responseData).toEqual('bar')
    })
    it('should resolve with the response body of a API call if no dataPath', async () => {
      const apiCall = { url: 'https://foo.bar' }
      axios.get.mockResolvedValue({ data: { foo: 'bar' } })

      const responseData = await getOutput(apiCall)

      expect(responseData).toEqual({ foo: 'bar' })
    })
  })
})
