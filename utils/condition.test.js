import { checkCondition, getOperatorSymbol } from './condition'
import { getOutput } from './api'

jest.mock('./api', () => {
  return {
    getOutput: jest.fn()
  }
})

describe('condition', () => {
  describe('checkCondition', () => {
    it('should check conditions for strings', async () => {
      getOutput.mockImplementation(() => Promise.resolve('abc'))

      const equalTrue = await checkCondition({}, 'EQUALS', 'abc')
      expect(equalTrue).toBe(true)

      const equalFalse = await checkCondition({}, 'EQUALS', 'foo')
      expect(equalFalse).toBe(false)

      const greaterThanTrue = await checkCondition({}, 'GREATER_THAN', 'aaa')
      expect(greaterThanTrue).toBe(true)

      const greaterThanFalse = await checkCondition({}, 'GREATER_THAN', 'zzz')
      expect(greaterThanFalse).toBe(false)

      const lessThanTrue = await checkCondition({}, 'LESS_THAN', 'zzz')
      expect(lessThanTrue).toBe(true)

      const lessThanFalse = await checkCondition({}, 'LESS_THAN', 'aaa')
      expect(lessThanFalse).toBe(false)
    })
    it('should check conditions for integers', async () => {
      getOutput.mockImplementation(() => Promise.resolve(10))

      const equalTrue = await checkCondition({}, 'EQUALS', '10')
      expect(equalTrue).toBe(true)

      const equalFalse = await checkCondition({}, 'EQUALS', '11')
      expect(equalFalse).toBe(false)

      const greaterThanTrue = await checkCondition({}, 'GREATER_THAN', '9')
      expect(greaterThanTrue).toBe(true)

      const greaterThanFalse = await checkCondition({}, 'GREATER_THAN', '11')
      expect(greaterThanFalse).toBe(false)

      const lessThanTrue = await checkCondition({}, 'LESS_THAN', '11')
      expect(lessThanTrue).toBe(true)

      const lessThanFalse = await checkCondition({}, 'LESS_THAN', '9')
      expect(lessThanFalse).toBe(false)
    })
    it('should return undefined for invalid operators', async () => {
      getOutput.mockImplementation(() => Promise.resolve(10))

      const invalidOperator = await checkCondition({}, 'FOO_BAR', '10')
      expect(invalidOperator).toBe(undefined)
    })
    it('should return the api result and condition result if returnApiResult param is true', async () => {
      getOutput.mockImplementation(() => Promise.resolve(10))

      const { conditionResult, apiResult } = await checkCondition({}, 'EQUALS', '10', true)
      expect(conditionResult).toBe(true)
      expect(apiResult).toBe(10)
    })
  })
  describe('getOperatorSymbol', () => {
    it('should return a value for the EQUALS operator', () => {
      const result = getOperatorSymbol('EQUALS')
      expect(result).toBe('equals')
    })
    it('should return a value for the GREATER_THAN operator', () => {
      const result = getOperatorSymbol('GREATER_THAN')
      expect(result).toBe('is greater than')
    })
    it('should return a value for the LESS_THAN operator', () => {
      const result = getOperatorSymbol('LESS_THAN')
      expect(result).toBe('is less than')
    })
    it('should return a empty string for the an invalid operator', () => {
      const result = getOperatorSymbol('FOO_BAR')
      expect(result).toBe('')
    })
  })
})
