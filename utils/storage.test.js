import AsyncStorage from '@react-native-async-storage/async-storage'
import { saveItem, getItem, getItems, deleteItems } from './storage'

jest.mock('@react-native-async-storage/async-storage')

describe('storage', () => {
  beforeEach(() => {
    AsyncStorage.getItem.mockReset()
    AsyncStorage.setItem.mockReset()
  })
  describe('saveItem', () => {
    it('should add new a item to the collection if not already present', async () => {
      AsyncStorage.getItem.mockResolvedValue('[{"id":1,"bar":"qux"}]')
      AsyncStorage.setItem.mockResolvedValue()

      await saveItem('foo', { id: 2, bar: 'baz' })

      expect(AsyncStorage.getItem).toHaveBeenLastCalledWith('foo')
      expect(AsyncStorage.setItem).toHaveBeenLastCalledWith('foo', '[{"id":1,"bar":"qux"},{"id":2,"bar":"baz"}]')
    })
    it('should add new a item to the collection if no items present', async () => {
      AsyncStorage.getItem.mockResolvedValue(/* no items */)
      AsyncStorage.setItem.mockResolvedValue()

      await saveItem('foo', { id: 1, bar: 'qux' })

      expect(AsyncStorage.getItem).toHaveBeenLastCalledWith('foo')
      expect(AsyncStorage.setItem).toHaveBeenLastCalledWith('foo', '[{"id":1,"bar":"qux"}]')
    })
    it('should update existing an item in the collection if it exists', async () => {
      AsyncStorage.getItem.mockResolvedValue('[{"id":1,"bar":"qux"}]')
      AsyncStorage.setItem.mockResolvedValue()

      await saveItem('foo', { id: 1, bar: 'baz' })

      expect(AsyncStorage.getItem).toHaveBeenLastCalledWith('foo')
      expect(AsyncStorage.setItem).toHaveBeenLastCalledWith('foo', '[{"id":1,"bar":"baz"}]')
    })
  })
  describe('getItem', () => {
    it('should get the item from the collection by id', async () => {
      AsyncStorage.getItem.mockResolvedValue('[{"id":1,"bar":"baz"}]')

      const item = await getItem('foo', 1)

      expect(item).toEqual({ id: 1, bar: 'baz' })
    })
    it('should return null if the item does not exist', async () => {
      AsyncStorage.getItem.mockResolvedValue('[{"id":1,"bar":"baz"}]')

      const item = await getItem('foo', 2)

      expect(item).toEqual(null)
    })
    it('should return null if no items exist', async () => {
      AsyncStorage.getItem.mockResolvedValue()

      const item = await getItem('foo', 1)

      expect(item).toEqual(null)
    })
  })
  describe('getItems', () => {
    it('should fetch and parse items from a collection', async () => {
      AsyncStorage.getItem.mockResolvedValue('[{"id":1,"baz":"qux"}]')

      const items = await getItems('foo')

      expect(AsyncStorage.getItem).toHaveBeenLastCalledWith('foo')
      expect(items).toEqual([{ id: 1, baz: 'qux' }])
    })
    it('should return an empty array of no items stored', async () => {
      AsyncStorage.getItem.mockResolvedValue(null)

      const items = await getItems('foo')

      expect(AsyncStorage.getItem).toHaveBeenLastCalledWith('foo')
      expect(items).toEqual([])
    })
  })
  describe('deleteItems', () => {
    it('should delete items with matching ids', async () => {
      AsyncStorage.getItem.mockResolvedValue('[{"id":1,"foo":"bar"},{"id":2,"baz":"qux"}]')
      AsyncStorage.setItem.mockResolvedValue()

      await deleteItems('foo', [2])

      expect(AsyncStorage.getItem).toHaveBeenLastCalledWith('foo')
      expect(AsyncStorage.setItem).toHaveBeenLastCalledWith('foo', '[{"id":1,"foo":"bar"}]')
    })
    it('should not delete any items empty array of ids passed', async () => {
      AsyncStorage.getItem.mockResolvedValue('[{"id":1,"foo":"bar"},{"id":2,"baz":"qux"}]')
      AsyncStorage.setItem.mockResolvedValue()

      await deleteItems('foo', [])

      expect(AsyncStorage.getItem).not.toHaveBeenCalled()
      expect(AsyncStorage.setItem).not.toHaveBeenCalled()
    })
    it('should not delete any items if no ids passed', async () => {
      AsyncStorage.getItem.mockResolvedValue('[{"id":1,"foo":"bar"},{"id":2,"baz":"qux"}]')
      AsyncStorage.setItem.mockResolvedValue()

      await deleteItems('foo')

      expect(AsyncStorage.getItem).not.toHaveBeenCalled()
      expect(AsyncStorage.setItem).not.toHaveBeenCalled()
    })
    it('should not delete any items if no items stored', async () => {
      AsyncStorage.getItem.mockResolvedValue()
      AsyncStorage.setItem.mockResolvedValue()

      await deleteItems('foo', [1, 2, 3])

      expect(AsyncStorage.getItem).toHaveBeenLastCalledWith('foo')
      expect(AsyncStorage.setItem).not.toHaveBeenCalled()
    })
  })
})
