import { execute, getRootNode, getChildren } from './block'
import { checkCondition } from './condition'
import { getItem } from './storage'

jest.mock('./storage', () => {
  return { getItem: jest.fn() }
})
jest.mock('./condition', () => {
  return { checkCondition: jest.fn() }
})

describe('execute', () => {
  it('should an error for invalid nodes', async () => {
    const result = await execute({ type: 'foobar' })
    expect(result).toBe('Error: invalid node')
  })
  it('should return the value for a output node', async () => {
    const result = await execute({
      type: 'output',
      value: 'hello'
    })
    expect(result).toBe('hello')
  })
  it('should return an empty string for a output node with no value', async () => {
    const result = await execute({
      type: 'output'
    })
    expect(result).toBe('')
  })
  it('should return an empty string for a if node with no condition', async () => {
    const result = await execute({
      type: 'if'
    })
    expect(result).toBe('')
  })
  it('should return an empty string for a if node with no branches', async () => {
    const result = await execute({
      type: 'if',
      condition: 1,
      apiId: 1
    })
    expect(result).toBe('')
  })
  it('should return execute a if nodes true branch if the condition evaluates to true', async () => {
    checkCondition.mockImplementation(() => Promise.resolve(true))
    getItem.mockImplementation((collection, id) => {
      if (collection === 'conditions' || collection === 'api_calls') {
        return Promise.resolve({ /* don't really care what these return */ })
      } else if (collection === 'nodes') {
        if (id === 2) {
          return Promise.resolve({
            id: 2,
            type: 'output',
            value: 'something is true'
          })
        } else if (id === 3) {
          return Promise.resolve({
            id: 2,
            type: 'output',
            value: 'something is false'
          })
        }
      }
    })

    const result = await execute({
      type: 'if',
      condition: 1,
      trueBranch: 2,
      falseBranch: 3
    })
    expect(result).toBe('something is true')
  })
  it('should return execute a if nodes false branch if the condition evaluates to false', async () => {
    checkCondition.mockImplementation(() => Promise.resolve(false))
    getItem.mockImplementation((collection, id) => {
      if (collection === 'conditions' || collection === 'api_calls') {
        return Promise.resolve({ /* don't really care what these return */ })
      } else if (collection === 'nodes') {
        if (id === 2) {
          return Promise.resolve({
            id: 2,
            type: 'output',
            value: 'something is true'
          })
        } else if (id === 3) {
          return Promise.resolve({
            id: 2,
            type: 'output',
            value: 'something is false'
          })
        }
      }
    })

    const result = await execute({
      type: 'if',
      condition: 1,
      trueBranch: 2,
      falseBranch: 3
    })
    expect(result).toBe('something is false')
  })
})

describe('getRootNode', () => {
  it('should return the current node if it has no parent', async () => {
    const rootNode = await getRootNode({
      id: 1
    })
    expect(rootNode).toStrictEqual({ id: 1 })
  })
  it('should return the root node where the current node has parent(s)', async () => {
    getItem.mockImplementation((collection, id) => {
      if (collection === 'nodes') {
        if (id === 2) {
          return Promise.resolve({
            id: 2,
            type: 'output',
            value: 'hello'
          })
        }
      }
    })

    const rootNode = await getRootNode({
      id: 1,
      parent: 2
    })
    expect(rootNode).toStrictEqual({
      id: 2,
      type: 'output',
      value: 'hello'
    })
  })
})

describe('getChildren', () => {
  it('should return an empty array for output nodes', async () => {
    const children = await getChildren({
      type: 'output',
      value: 'foo'
    })

    expect(children).toEqual([])
  })
  it('should return an empty if nodes with no true or false branches', async () => {
    const children = await getChildren({
      type: 'if'
    })

    expect(children).toEqual([])
  })
  it('should return an empty array for invalid node types', async () => {
    const children = await getChildren({
      type: 'foobar'
    })

    expect(children).toEqual([])
  })
  it('should return an an if nodes branches', async () => {
    getItem.mockImplementation((collection, id) => {
      if (collection === 'nodes') {
        if (id === 2) {
          return Promise.resolve({
            id: 2,
            type: 'output',
            value: 'foo'
          })
        }
        if (id === 3) {
          return Promise.resolve({
            id: 2,
            type: 'output',
            value: 'bar'
          })
        }
      }
    })

    const children = await getChildren({
      id: 1,
      type: 'if',
      trueBranch: 2,
      falseBranch: 3
    })

    expect(children).toEqual([2, 3])
  })
})
