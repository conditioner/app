import React from 'react'
import { Pressable, Text, StyleSheet } from 'react-native'
import { getOperatorSymbol } from '../../../utils/condition'

export default function ConditionItem ({ id, apiId, apiTitle, operator, value, navigation }) {
  const styles = StyleSheet.create({
    container: {
      margin: 10,
      marginTop: 0,
      padding: 10,
      borderRadius: 4,
      backgroundColor: '#FFF',
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.20,
      shadowRadius: 1.41,

      elevation: 2
    },
    api: {
      fontSize: 18,
      textAlign: 'center',
      marginBottom: 5
    },
    operator: {
      textAlign: 'center',
      color: '#888888'
    },
    value: {
      fontSize: 18,
      textAlign: 'center',
      marginTop: 5
    }
  })

  return (
    <Pressable testID='conditionListItem' style={styles.container} onPress={() => navigation.navigate('ConditionEdit', { item: { id, apiId, operator, value } })}>
      <Text testID='conditionApi' style={styles.api}>{apiTitle || apiId}</Text>
      <Text testID='conditionOperator' style={styles.operator}>{getOperatorSymbol(operator)}</Text>
      <Text testID='conditionValue' style={styles.value}>{value}</Text>
    </Pressable>
  )
}
