import React from 'react'
import { render, fireEvent } from '@testing-library/react-native'
import ConditionItem from './index'

describe('<ConditionItem />', () => {
  it('should render a condition items attributes', () => {
    const { getByTestId } = render(<ConditionItem id={123} apiId={456} apiTitle='example api' operator='GREATER_THAN' value='10' />)
    const title = getByTestId('conditionApi').children[0]
    const operator = getByTestId('conditionOperator').children[0]
    const value = getByTestId('conditionValue').children[0]
    expect(title).toBe('example api')
    expect(operator).toBe('is greater than')
    expect(value).toBe('10')
  })
  it('should use a API Id if not title is set', () => {
    const { getByTestId } = render(<ConditionItem id={123} apiId={456} operator='GREATER_THAN' value='10' />)
    const title = getByTestId('conditionApi').children[0]
    expect(title).toBe('456')
  })
  it('should navigate to ConditionEdit when tapped', () => {
    const navigation = {
      navigate: jest.fn()
    }
    const { getByTestId } = render(<ConditionItem id={123} apiId={456} apiTitle='example api' operator='GREATER_THAN' value='10' navigation={navigation} />)
    fireEvent.press(getByTestId('conditionListItem'))
    expect(navigation.navigate).toHaveBeenLastCalledWith('ConditionEdit', {
      item: {
        id: 123,
        apiId: 456,
        operator: 'GREATER_THAN',
        value: '10'
      }
    })
  })
})
