import React, { useState, useEffect } from 'react'
import { View, Text, FlatList, StyleSheet } from 'react-native'
import { useFocusEffect } from '@react-navigation/native'
import get from 'lodash.get'
import { getItems } from '../../utils/storage'
import ConditionItem from './ConditionItem'
import NoItems from '../NoItems'
import Button from '../Button'

export default function Conditions ({ navigation }) {
  const styles = StyleSheet.create({
    container: {
      flexGrow: 1
    }
  })

  const [loading, setLoading] = useState(true)
  const [conditions, setConditions] = useState([])

  useEffect(() => {
    fetchData()
  }, [])

  useFocusEffect(
    React.useCallback(() => {
      fetchData()
    }, [])
  )

  async function fetchData () {
    setLoading(true)
    return Promise.all([getItems('conditions'), getItems('api_calls')])
      .then(data => {
        const [conditions, apiCalls] = data
        const merged = conditions.map(condition => {
          const apiCall = apiCalls.find(a => a.id === condition.apiId)
          const apiTitle = get(apiCall, 'title', 'Untitled API Call')
          return Object.assign({}, condition, { apiTitle })
        })
        setConditions(merged)
      })
      .catch(console.log)
      .finally(() => setLoading(false))
  }

  if (loading) {
    return (<Text>Loading...</Text>)
  }

  return (
    <View style={styles.container}>
      <Button testID='createCondition' title='Add Condition' onPress={() => navigation.navigate('ConditionEdit')} />
      {conditions.length === 0
        ? <NoItems message='No Conditions' />
        : <FlatList
            data={conditions}
            renderItem={({ item }) => <ConditionItem navigation={navigation} {...item} />}
            keyExtractor={item => item.id.toString()}
          />}
    </View>
  )
}
