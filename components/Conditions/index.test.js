import React from 'react'
import { render, waitFor, fireEvent } from '@testing-library/react-native'
import Conditions from './index'
import { getItems } from '../../utils/storage'

jest.mock('@react-navigation/native')
jest.mock('../../utils/storage', () => {
  return {
    getItems: jest.fn(),
    getItem: jest.fn()
  }
})

describe('<Conditions />', () => {
  it('should fetch all conditions to display', async () => {
    getItems.mockImplementation((collection) => {
      if (collection === 'conditions') {
        return Promise.resolve([{
          id: 1,
          apiId: 1,
          operator: 'EQUALS',
          value: 'foo'
        }, {
          id: 2,
          apiId: 2,
          operator: 'GREATER_THAN',
          value: '10'
        }])
      } else if (collection === 'api_calls') {
        return Promise.resolve([{
          id: 1,
          title: 'foo api call'
        }, {
          id: 2,
          title: 'bar'
        }])
      }
    })

    const { queryAllByTestId, queryByTestId } = render(<Conditions />)
    await waitFor(() => queryAllByTestId('conditionListItem'))

    const noItems = queryByTestId('noItems')
    const items = queryAllByTestId('conditionListItem')

    expect(noItems).toBeNull()
    expect(items.length).toEqual(2)
  })
  it('should show no items message if no condition items are saved', async () => {
    getItems.mockImplementation((collection) => {
      if (collection === 'conditions') {
        return Promise.resolve([])
      } else if (collection === 'api_calls') {
        return Promise.resolve([{
          id: 1,
          title: 'foo api call'
        }, {
          id: 2,
          title: 'bar'
        }])
      }
    })

    const { getByTestId, queryAllByTestId, queryByTestId } = render(<Conditions />)
    await waitFor(() => getByTestId('noItems'))

    const noItems = queryByTestId('noItems')
    const items = queryAllByTestId('conditionListItem')

    expect(items.length).toEqual(0)
    expect(noItems).toBeTruthy()
  })
  it('should navigate to ConditionEdit when an item is tapped', async () => {
    getItems.mockImplementation((collection) => {
      if (collection === 'conditions') {
        return Promise.resolve([])
      } else if (collection === 'api_calls') {
        return Promise.resolve([])
      }
    })
    const navigation = {
      navigate: jest.fn()
    }
    const { getByTestId } = render(<Conditions navigation={navigation} />)
    await waitFor(() => getByTestId('createCondition'))

    fireEvent.press(getByTestId('createCondition'))
    expect(navigation.navigate).toHaveBeenLastCalledWith('ConditionEdit')
  })
})
