import React, { useState, useEffect } from 'react'
import { View, Text, TextInput, SafeAreaView, StyleSheet } from 'react-native'
import get from 'lodash.get'
import { getItems, saveItem } from '../../../utils/storage'
import { conditions, checkCondition } from '../../../utils/condition'
import ModalPicker from '../../ModalPicker'
import ActionButtons from '../../ActionButtons'
import ConditionOutput from './ConditionOutput'

export default function ConditionEdit ({ route }) {
  const styles = StyleSheet.create({
    container: {
      flexGrow: 1,
      padding: 15
    },
    label: {
      fontWeight: '700',
      marginBottom: 10
    },
    input: {
      borderColor: '#E8E8E8',
      borderWidth: 1,
      borderRadius: 4,
      backgroundColor: '#FFFFFF',
      height: 48,
      marginBottom: 20,
      padding: 10
    }
  })

  // we can either be updating or creating a new condition
  const item = get(route, 'params.item', {
    id: Date.now()
  })

  const [loading, setLoading] = useState(true)
  const [condition, setCondition] = useState(item)
  const [apiId, setApiId] = useState(get(item, 'apiId'))
  const [operator, setOperator] = useState(get(item, 'operator'))
  const [value, setValue] = useState(get(item, 'value'))
  const [apiCalls, setApiCalls] = useState([])
  const [apiCallTitle, setApiCallTitle] = useState('')
  const [result, setResult] = useState(null)

  useEffect(() => {
    setLoading(true)
    getItems('api_calls')
      .then(setApiCalls)
      .catch(console.log)
      .finally(() => setLoading(false))
  }, [])

  useEffect(() => {
    setCondition(Object.assign({}, condition, { apiId, operator, value }))
  }, [apiId, operator, value])

  async function save () {
    try {
      await saveItem('conditions', condition)
    } catch (e) {
      console.log(e)
    }
  }

  async function test () {
    try {
      const apiCall = apiCalls.find(a => a.id === apiId)
      const { conditionResult, apiResult } = await checkCondition(apiCall, operator, value, true)
      setApiCallTitle(`${apiCall.title} (${apiResult})`)
      setResult(conditionResult)
    } catch (e) {
      console.log(e)
    }
  }

  if (loading) {
    return (<Text>Loading...</Text>)
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View style={styles.container}>
        <Text style={styles.label}>API Call</Text>
        <ModalPicker
          label='API Call'
          options={apiCalls}
          selectedValue={apiId}
          onChange={setApiId}
        />
        <Text style={styles.label}>Relational Operator</Text>
        <ModalPicker
          label='Relational Operator'
          options={Object.keys(conditions)
            .map(key => ({ id: key, title: conditions[key].stringValue }))}
          selectedValue={operator}
          onChange={setOperator}
        />
        <Text style={styles.label}>Value</Text>
        <TextInput
          autoCapitalize='none'
          style={styles.input}
          onChangeText={setValue}
          value={value}
          autoCompleteType='off'
        />
        <ConditionOutput condition={apiCallTitle} operator={operator} operand={value} result={result} />
      </View>
      <ActionButtons onTest={test} onSave={save} />
    </SafeAreaView>
  )
}
