import React from 'react'
import { render, waitFor, fireEvent, act } from '@testing-library/react-native'
import ConditionEdit from './index'
import { checkCondition } from '../../../utils/condition'
import { getItems, saveItem } from '../../../utils/storage'

jest.mock('../../../utils/condition', () => {
  return {
    checkCondition: jest.fn().mockReturnValueOnce({ conditionResult: true, apiResult: 10 }),
    conditions: {
      EQUALS: {
        stringValue: 'equals',
        evaluate: (a, b) => a === b
      },
      GREATER_THAN: {
        stringValue: 'is greater than',
        evaluate: (a, b) => a > b
      },
      LESS_THAN: {
        stringValue: 'is less than',
        evaluate: (a, b) => a < b
      }
    }
  }
})
jest.mock('../../../utils/storage', () => {
  return {
    getItems: jest.fn(),
    saveItem: jest.fn()
  }
})

describe('<ConditionEdit />', () => {
  // mock getItems('api_calls') to return a fake api call record
  getItems.mockImplementation(() => Promise.resolve([{
    id: 123,
    url: 'https://example.com/foo/bar'
  }]))

  it('should call the set api call when test is tapped', async () => {
    const route = {
      params: {
        item: {
          apiId: 123,
          operator: 'EQUALS',
          value: '10'
        }
      }
    }
    const { getByTestId } = render(<ConditionEdit route={route} />)
    await act(async () => {
      await waitFor(() => getByTestId('testButton'))
      fireEvent.press(getByTestId('testButton'))
    })
    expect(checkCondition).toHaveBeenCalledTimes(1)
    expect(checkCondition).toHaveBeenLastCalledWith({
      id: 123,
      url: 'https://example.com/foo/bar'
    }, 'EQUALS', '10', true)
  })
  it('should save the condition when save is tapped', async () => {
    const route = {
      params: {
        item: {
          apiId: 123,
          operator: 'EQUALS',
          value: '10'
        }
      }
    }
    const { getByTestId } = render(<ConditionEdit route={route} />)
    await waitFor(() => getByTestId('saveButton'))
    fireEvent.press(getByTestId('saveButton'))
    expect(saveItem).toHaveBeenCalledTimes(1)
    expect(saveItem).toHaveBeenLastCalledWith('conditions', {
      apiId: 123,
      operator: 'EQUALS',
      value: '10'
    })
  })
})
