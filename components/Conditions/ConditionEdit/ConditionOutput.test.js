import React from 'react'
import { render } from '@testing-library/react-native'
import ConditionOutput from './ConditionOutput'

describe('<ConditionOutput />', () => {
  it('should display a message to tap test if no props', () => {
    const { getByTestId } = render(<ConditionOutput />)
    expect(getByTestId('noConditionOutput')).toBeTruthy()
  })
  it('should display a the condition output if props passed', () => {
    const { getByTestId } = render(<ConditionOutput condition='foo' operator='EQUALS' operand='bar' />)
    expect(getByTestId('conditionOutput')).toBeTruthy()
  })
  it('should display "false" output if result is false', () => {
    const { getByTestId } = render(<ConditionOutput condition='foo' operator='EQUALS' operand='bar' result={false} />)
    expect(getByTestId('conditionOutputValue').children[0]).toBe('false')
  })
  it('should display "true" output if result is true', () => {
    const { getByTestId } = render(<ConditionOutput condition='foo' operator='EQUALS' operand='bar' result />)
    expect(getByTestId('conditionOutputValue').children[0]).toBe('true')
  })
})
