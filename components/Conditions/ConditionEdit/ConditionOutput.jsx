import React from 'react'
import { View, Text, StyleSheet, Platform } from 'react-native'
import { conditions } from '../../../utils/condition'

export default function ConditionEdit ({ condition, operator, operand, result }) {
  const styles = StyleSheet.create({
    pendingResponseMessage: {
      fontSize: 16,
      color: '#CCCCCC'
    },
    responseBox: {
      padding: 10,
      alignItems: 'center',
      justifyContent: 'center',
      borderColor: '#E8E8E8',
      borderWidth: 1,
      borderRadius: 4,
      backgroundColor: '#FFFFFF',
      minHeight: 100
    },
    responseMessage: {
      fontSize: 16,
      textAlign: 'center'
    },
    responseValue: {
      fontSize: 16,
      fontFamily: Platform.OS === 'ios' ? 'Courier' : 'monospace',
      fontWeight: '700',
      color: '#EE2678',
      textAlign: 'center'
    }
  })

  if (condition && operator && operand) {
    return (
      <View testID='conditionOutput' style={styles.responseBox}>
        <Text style={styles.responseMessage}>{condition} {conditions[operator].symbol} {operand} is </Text>
        <Text testID='conditionOutputValue' style={styles.responseValue}>{result ? 'true' : 'false'}</Text>
      </View>
    )
  }
  return (
    <View testID='noConditionOutput' style={styles.responseBox}>
      <Text style={styles.pendingResponseMessage}>Tap "Test" to see the condition result</Text>
    </View>
  )
}
