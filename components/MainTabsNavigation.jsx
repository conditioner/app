import React from 'react'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Blocks from './Blocks'
import Conditions from './Conditions'
import ApiCalls from './ApiCalls'

const Tab = createBottomTabNavigator()

export default function MainTabsNavigation ({ navigation, route }) {
  return (
    <Tab.Navigator
      initialRouteName='Blocks'
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color, size }) => {
          if (route.name === 'Blocks') {
            return <MaterialCommunityIcons name='code-braces' size={size} color={color} />
          } else if (route.name === 'Conditions') {
            return <MaterialCommunityIcons name='call-split' size={size} color={color} />
          } else if (route.name === 'ApiCalls') {
            return <MaterialCommunityIcons name='web' size={size} color={color} />
          }
        }
      })}
    >
      <Tab.Screen
        name='Blocks'
        component={Blocks}
        options={{
          tabBarLabel: 'Blocks'
        }}
      />
      <Tab.Screen
        name='Conditions'
        component={Conditions}
        options={{
          tabBarLabel: 'Conditions',
          title: 'hello'
        }}
      />
      <Tab.Screen
        name='ApiCalls'
        component={ApiCalls}
        options={{
          tabBarLabel: 'API Calls'
        }}
      />
    </Tab.Navigator>
  )
}
