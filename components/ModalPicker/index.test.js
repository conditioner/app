import React from 'react'
import { render, fireEvent } from '@testing-library/react-native'
import ModalPicker from './index'

describe('<ModalPicker />', () => {
  const mockOptions = [{
    id: 123,
    title: 'Foo'
  }, {
    id: 456
    // no title
  }, {
    id: 789,
    title: 'Bar'
  }]
  it('should display the title of the currently selected item', () => {
    const { queryByTestId } = render(<ModalPicker options={mockOptions} selectedValue={123} />)
    const title = queryByTestId('selectedItem')
    expect(title.children[0]).toBe('Foo')
  })
  it('should display the a default title of the currently selected item if it has no title', () => {
    const { queryByTestId } = render(<ModalPicker options={mockOptions} selectedValue={456} />)
    const title = queryByTestId('selectedItem')
    expect(title.children[0]).toBe(' ')
  })
  it('should open the selection modal when the currently selected item is tapped', () => {
    const { getByTestId } = render(<ModalPicker options={mockOptions} selectedValue={123} />)
    const modal = getByTestId('pickerModal')
    expect(modal.props.visible).toBe(false)
    fireEvent.press(getByTestId('selectedItem'))
    expect(modal.props.visible).toBe(true)
  })
  it('should list titles of selectable items in the modal', () => {
    const { queryByTestId } = render(<ModalPicker options={mockOptions} />)
    const options = queryByTestId('pickerOptions')
    expect(options.children.length).toBe(3)
  })
  it('should update the selected item if one is tapped in the modal', () => {
    const { getByTestId, queryByTestId } = render(<ModalPicker options={mockOptions} selectedValue={123} />)
    const title = queryByTestId('selectedItem')
    const options = queryByTestId('pickerOptions')
    fireEvent.press(getByTestId('selectedItem'))
    fireEvent.press(options.children[2])
    expect(title.children[0]).toBe('Foo')
  })
  it('should call onChange when an item is selected', () => {
    const onChange = jest.fn()
    const { getByTestId, queryByTestId } = render(<ModalPicker options={mockOptions} selectedValue={123} onChange={onChange} />)
    const options = queryByTestId('pickerOptions')
    fireEvent.press(getByTestId('selectedItem'))
    fireEvent.press(options.children[2])
    expect(onChange).toHaveBeenLastCalledWith(789)
  })
  it('should dismiss the selection modal when an item is selected', () => {
    const { getByTestId, queryByTestId } = render(<ModalPicker options={mockOptions} selectedValue={123} />)
    const modal = getByTestId('pickerModal')
    const options = queryByTestId('pickerOptions')
    fireEvent.press(getByTestId('selectedItem'))
    fireEvent.press(options.children[2])
    expect(modal.props.visible).toBe(false)
  })
  it('should show no selected item if none is passed', () => {
    const { queryByTestId } = render(<ModalPicker options={mockOptions} />)
    const title = queryByTestId('selectedItem')
    expect(title.children[0]).toBe(' ')
  })
})
