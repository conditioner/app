import React, { useState } from 'react'
import { Pressable, View, Text, Modal, SafeAreaView, StyleSheet } from 'react-native'
import get from 'lodash.get'

export default function ModalPicker ({ label, options, selectedValue, onChange }) {
  const styles = StyleSheet.create({
    selectItem: {
      backgroundColor: '#E1E1E1',
      borderRadius: 10,
      marginBottom: 10
    },
    selectItemText: {
      fontSize: 20,
      padding: 8,
      textAlign: 'center',
      color: '#333333'
    },
    modalView: {
      backgroundColor: '#FFFFFF',
      flexGrow: 1
    },
    title: {
      padding: 15,
      paddingTop: 30,
      paddingBottom: 20
    },
    titleText: {
      fontSize: 30
    },
    optionList: {
      borderColor: '#E1E1E1',
      borderTopWidth: 1
    },
    modalContent: {
      padding: 15
    }
  })

  const [open, setOpen] = useState(false)

  function getItemTitle () {
    if (!selectedValue) {
      return ' '
    }
    const item = options.find(option => option.id === selectedValue)
    return get(item, 'title', ' ')
  }

  function handleItemSelect (value) {
    onChange && onChange(value)
    setOpen(false)
  }

  return (
    <>
      <Pressable style={styles.selectItem} onPress={() => setOpen(true)}>
        <Text testID='selectedItem' style={styles.selectItemText}>{getItemTitle()}</Text>
      </Pressable>
      <Modal
        testID='pickerModal'
        animationType='slide'
        transparent
        visible={open}
        onRequestClose={() => setOpen(!open)}
      >
        <SafeAreaView style={styles.modalView}>
          <View style={styles.title}>
            <Text style={styles.titleText}>{label || 'Select One'}</Text>
          </View>
          <View style={styles.modalContent}>
            <View testID='pickerOptions' style={styles.optionList}>
              {options.map(option => <Option key={option.id} title={option.title} onPress={() => handleItemSelect(option.id)} />)}
            </View>
          </View>
        </SafeAreaView>
      </Modal>
    </>
  )
}

const Option = ({ title, onPress }) => {
  const styles = StyleSheet.create({
    item: {
      borderColor: '#E1E1E1',
      borderBottomWidth: 1
    },
    itemText: {
      paddingTop: 18,
      paddingBottom: 18,
      paddingLeft: 5,
      paddingRight: 5,
      fontSize: 18,
      color: '#333333'
    }
  })

  return (
    <Pressable style={styles.item} onPress={onPress}>
      <Text style={styles.itemText}>{title}</Text>
    </Pressable>
  )
}
