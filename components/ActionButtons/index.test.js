import React from 'react'
import { render, fireEvent } from '@testing-library/react-native'
import ActionButtons from './index'

describe('<ActionButtons />', () => {
  it('should call onTest when the test button is pressed', () => {
    const onTest = jest.fn()
    const { getByTestId } = render(<ActionButtons onTest={onTest} />)
    fireEvent.press(getByTestId('testButton'))
    expect(onTest).toHaveBeenCalledTimes(1)
  })

  it('should call onSave when the save button is pressed', () => {
    const onSave = jest.fn()
    const { getByTestId } = render(<ActionButtons onSave={onSave} />)
    fireEvent.press(getByTestId('saveButton'))
    expect(onSave).toHaveBeenCalledTimes(1)
  })
})
