import React from 'react'
import { View, StyleSheet } from 'react-native'
import Button from '../Button'

export default function ActionButtons ({ onTest, onSave }) {
  const styles = StyleSheet.create({
    buttonBar: {
      flexDirection: 'row'
    },
    testButton: {
      marginLeft: 10,
      marginRight: 5,
      flexGrow: 1
    },
    saveButton: {
      marginLeft: 5,
      marginRight: 10,
      flexGrow: 1
    }
  })

  return (
    <View style={styles.buttonBar}>
      <Button onPress={onTest} testID='testButton' title='Test' theme='light' style={styles.testButton} />
      <Button onPress={onSave} testID='saveButton' title='Save' style={styles.saveButton} />
    </View>
  )
}
