import React from 'react'
import { render, waitFor, fireEvent } from '@testing-library/react-native'
import ApiCalls from './index'
import { getItems } from '../../utils/storage'

jest.mock('@react-navigation/native')
jest.mock('../../utils/storage', () => {
  return {
    getItems: jest.fn()
  }
})

describe('<ApiCalls />', () => {
  it('should fetch all API calls to display', async () => {
    getItems.mockImplementation(() => Promise.resolve([{ id: 1, title: 'foo' }, { id: 2, title: 'bar' }]))

    const { queryAllByTestId, queryByTestId } = render(<ApiCalls />)
    await waitFor(() => queryAllByTestId('apiCallListItem'))

    const noItems = queryByTestId('noItems')
    const items = queryAllByTestId('apiCallListItem')

    expect(noItems).toBeNull()
    expect(items.length).toEqual(2)
  })
  it('should show no items message if no API calls are saved', async () => {
    getItems.mockImplementation(() => Promise.resolve([]))

    const { getByTestId, queryAllByTestId, queryByTestId } = render(<ApiCalls />)
    await waitFor(() => getByTestId('noItems'))

    const noItems = queryByTestId('noItems')
    const items = queryAllByTestId('apiCallListItem')

    expect(items.length).toEqual(0)
    expect(noItems).toBeTruthy()
  })
  it('should navigate to ApiCallEdit when an item is tapped', async () => {
    const navigation = {
      navigate: jest.fn()
    }
    const { getByTestId } = render(<ApiCalls navigation={navigation} />)
    await waitFor(() => getByTestId('createApiCall'))

    fireEvent.press(getByTestId('createApiCall'))
    expect(navigation.navigate).toHaveBeenLastCalledWith('ApiCallEdit')
  })
})
