import React from 'react'
import { render, fireEvent } from '@testing-library/react-native'
import ApiCallItem from './index'

describe('<ApiCallItem />', () => {
  it('should display the API calls title', () => {
    const { queryByTestId } = render(<ApiCallItem title='Test Title' />)
    const title = queryByTestId('itemTitle')
    expect(title.children[0]).toBe('Test Title')
  })
  it('should fallback to the API endpoint if no title is set', () => {
    const { queryByTestId } = render(<ApiCallItem url='http://example.com/foo' />)
    const title = queryByTestId('itemTitle')
    expect(title.children[0]).toBe('http://example.com/foo')
  })
  it('should fallback to "untitled" no title or endpoint is set', () => {
    const { queryByTestId } = render(<ApiCallItem />)
    const title = queryByTestId('itemTitle')
    expect(title.children[0]).toBe('Untitled API Call')
  })
  it('should show the endpoint', () => {
    const { queryByTestId } = render(<ApiCallItem url='http://example.com/foo' />)
    const url = queryByTestId('itemUrl')
    expect(url.children[0]).toBe('http://example.com/foo')
  })
  it('should show API response data path', () => {
    const { queryByTestId } = render(<ApiCallItem dataPath='foo.bar' />)
    const dataPath = queryByTestId('dataPath')
    expect(dataPath.children[0]).toBe('foo.bar')
  })
  it('should not show an API response data path if not passed', () => {
    const { queryByTestId } = render(<ApiCallItem />)
    const dataPath = queryByTestId('dataPath')
    expect(dataPath).toBe(null)
  })
  it('should navigate to ApiCallEdit when tapped', () => {
    const navigation = {
      navigate: jest.fn()
    }
    const { getByTestId } = render(<ApiCallItem id={123} title='Test title' url='http://example.com/foo' dataPath='foo.bar' navigation={navigation} />)
    fireEvent.press(getByTestId('apiCallListItem'))
    expect(navigation.navigate).toHaveBeenLastCalledWith('ApiCallEdit', {
      item: {
        id: 123,
        title: 'Test title',
        url: 'http://example.com/foo',
        dataPath: 'foo.bar'
      }
    })
  })
})
