import React from 'react'
import { Pressable, View, Text, Platform, StyleSheet } from 'react-native'

export default function ApiCallItem ({ id, title, url, dataPath, navigation }) {
  const styles = StyleSheet.create({
    container: {
      margin: 10,
      marginTop: 0,
      padding: 10,
      borderRadius: 4,
      backgroundColor: '#FFF',
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.20,
      shadowRadius: 1.41,

      elevation: 2
    },
    title: {
      fontWeight: '700',
      fontSize: 16,
      marginBottom: 10
    }
  })

  function getTitle () {
    if (title && title.trim() !== '') {
      return title.trim()
    } else if (url && url.trim() !== '') {
      return url.trim()
    } else {
      return 'Untitled API Call'
    }
  }

  return (
    <Pressable testID='apiCallListItem' style={styles.container} onPress={() => navigation.navigate('ApiCallEdit', { item: { id, title, url, dataPath } })}>
      <Text testID='itemTitle' style={styles.title} numberOfLines={1}>{getTitle()}</Text>
      <Url url={url} />
      <DataPath dataPath={dataPath} />
    </Pressable>
  )
}

const Url = ({ url }) => {
  const styles = StyleSheet.create({
    url: {
      color: '#888888',
      marginBottom: 10
    },
    noUrl: {
      color: '#888888'
    }
  })

  if (url && url.trim() !== '') {
    return (
      <Text testID='itemUrl' style={styles.url}>{url}</Text>
    )
  }

  return <Text testID='itemUrl' style={styles.noUrl}>No URL</Text>
}

const DataPath = ({ dataPath }) => {
  const styles = StyleSheet.create({
    dataPathWrap: {
      backgroundColor: '#FBE5E1',
      padding: 5,
      borderRadius: 4,
      marginRight: 'auto'
    },
    dataPath: {
      color: '#C0341D',
      fontFamily: Platform.OS === 'ios' ? 'Courier' : 'monospace'
    }
  })

  if (dataPath && dataPath.trim() !== '') {
    return (
      <View style={styles.dataPathWrap}>
        <Text testID='dataPath' style={styles.dataPath}>{dataPath}</Text>
      </View>
    )
  }

  return null
}
