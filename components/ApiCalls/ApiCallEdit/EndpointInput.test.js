import React from 'react'
import { create } from 'react-test-renderer'
import { render, fireEvent } from '@testing-library/react-native'
import EndpointInput from './EndpointInput'

describe('<EndpointInput />', () => {
  it('should use a blank URL if none is passed', () => {
    const { root } = create(<EndpointInput />)
    const urlInput = root.findByType('TextInput')
    expect(urlInput.props.value).toBe('')
  })

  it('should use a the url prop as the URL if passed', () => {
    const { root } = create(<EndpointInput url='foo.bar' />)
    const urlInput = root.findByType('TextInput')
    expect(urlInput.props.value).toBe('foo.bar')
  })

  it('should call onChange when the url is updated', () => {
    const onChange = jest.fn()
    const { getByTestId } = render(<EndpointInput onChange={onChange} />)
    fireEvent.changeText(getByTestId('endpointUrl'), 'foo.bar')
    expect(onChange).toHaveBeenLastCalledWith('GET', 'foo.bar')
  })
})
