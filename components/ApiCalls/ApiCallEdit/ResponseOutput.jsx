import React from 'react'
import { View, ScrollView, Text, Pressable, StyleSheet, Platform } from 'react-native'
import get from 'lodash.get'

/**
 * Basically pretty print for JSON responses from API calls.
 */
export default function ResponseOutput ({ dataPath, response, setDataPath }) {
  const styles = StyleSheet.create({
    pendingResponse: {
      padding: 10,
      alignItems: 'center',
      justifyContent: 'center',
      borderColor: '#E8E8E8',
      borderWidth: 1,
      borderRadius: 4,
      backgroundColor: '#FFFFFF',
      minHeight: 300
    },
    pendingResponseMessage: {
      fontSize: 16,
      color: '#CCCCCC'
    },
    responseValue: {
      fontSize: 20,
      marginBottom: 15,
      textAlign: 'center'
    },
    responseJson: {
      padding: 15,
      paddingLeft: 10,
      paddingRight: 10,
      borderColor: '#E8E8E8',
      borderWidth: 1,
      borderRadius: 4,
      backgroundColor: '#FFFFFF',
      minHeight: 300,
      marginBottom: 20
    }
  })

  function getOutput () {
    const result = get(response, dataPath, 'undefined')

    if (typeof result === 'object') {
      return 'object'
    }
    return result
  }

  if (!response) {
    return (
      <View style={styles.pendingResponse}>
        <Text style={styles.pendingResponseMessage}>Tap "Test" to get a response</Text>
      </View>
    )
  }

  return (
    <View>
      <Text testID='responseOutput' style={styles.responseValue}>{getOutput(response, dataPath)}</Text>
      <ScrollView style={styles.responseJson} horizontal>
        <JSONObject json={response} dataPath={dataPath} setDataPath={setDataPath} />
      </ScrollView>
    </View>
  )
}

const JSONObject = ({ json, parentKey, dataPath, setDataPath, parentPath = '', depth = 0, trailingComma = false }) => {
  const styles = StyleSheet.create({
    indent: {
      marginLeft: depth * 10,
      flexDirection: 'row',
      fontSize: 16,
      fontFamily: Platform.OS === 'ios' ? 'Courier' : 'monospace'
    },
    indentChild: {
      marginLeft: (depth + 1) * 10,
      flexDirection: 'row',
      borderColor: '#FFFFFF',
      borderWidth: 1,
      borderRadius: 3,
      paddingTop: 2,
      paddingBottom: 2,
      marginRight: 'auto'
    },
    selectedIndentChild: {
      marginLeft: (depth + 1) * 10,
      flexDirection: 'row',
      borderColor: '#FF9900',
      borderWidth: 1,
      borderRadius: 3,
      paddingTop: 2,
      paddingBottom: 2,
      marginRight: 'auto'
    },
    default: {
      fontSize: 16,
      fontFamily: Platform.OS === 'ios' ? 'Courier' : 'monospace'
    },
    key: {
      color: '#CD8E62',
      fontSize: 16,
      fontFamily: Platform.OS === 'ios' ? 'Courier' : 'monospace'
    },
    string: {
      color: '#CD8E62',
      fontSize: 16,
      fontFamily: Platform.OS === 'ios' ? 'Courier' : 'monospace'
    },
    number: {
      color: '#6F8E3A',
      fontSize: 16,
      fontFamily: Platform.OS === 'ios' ? 'Courier' : 'monospace'
    }
  })

  const isArray = Array.isArray(json)

  const children = []
  const keyValues = Object.entries(json)
  let index = 0
  for (const [key, value] of keyValues) {
    index++
    const lastItem = keyValues.length === index
    if (typeof value === 'object' && value !== null) {
      children.push(
        <JSONObject key={key} json={value} dataPath={dataPath} setDataPath={setDataPath} depth={depth + 1} parentKey={isArray ? null : key} parentPath={`${parentPath}${key}.`} trailingComma={!lastItem} />
      )
    } else {
      const jsonValue = JSON.stringify(value)
      const isSelected = parentPath + key === dataPath
      children.push(
        <Pressable testID='responseKeyValue' key={key} style={isSelected ? styles.selectedIndentChild : styles.indentChild} onPress={() => setDataPath(parentPath + key)}>
          <Text style={styles.key}>"{key}"</Text>
          <Text style={styles.default}>: </Text>
          <Text style={jsonValue.startsWith('"') ? styles.string : styles.number}>{jsonValue}</Text>
          <Text style={styles.default}>{!lastItem && ','}</Text>
        </Pressable>
      )
    }
  }

  const openingBrace = isArray ? '[' : '{'
  const closingBrace = isArray ? ']' : '}'

  return (
    <View>
      <View style={styles.indent}>
        {parentKey && <Text style={styles.key}>{`"${parentKey}"`}</Text>}
        <Text style={styles.default}>{parentKey ? `: ${openingBrace}` : openingBrace}</Text>
      </View>
      {children}
      <Text style={styles.indent}>{trailingComma ? `${closingBrace},` : closingBrace}</Text>
    </View>
  )
}
