import React from 'react'
import { render, fireEvent } from '@testing-library/react-native'
import Response from './Response'

describe('<Response />', () => {
  it('should call onChange when the dataPath is updated', () => {
    const onChange = jest.fn()
    const { getByTestId } = render(<Response dataPath='foo' onChange={onChange} />)
    fireEvent.changeText(getByTestId('dataPathInput'), 'foo.bar')
    expect(onChange).toHaveBeenLastCalledWith({
      dataPath: 'foo.bar'
    })
  })
  it('should call onChange when the dataPath is first set', () => {
    const onChange = jest.fn()
    const { getByTestId } = render(<Response onChange={onChange} />)
    fireEvent.changeText(getByTestId('dataPathInput'), 'foo.bar')
    expect(onChange).toHaveBeenLastCalledWith({
      dataPath: 'foo.bar'
    })
  })
})
