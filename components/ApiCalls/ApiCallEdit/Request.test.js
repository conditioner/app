import React from 'react'
import { render, fireEvent } from '@testing-library/react-native'
import Request from './Request'

describe('<Request />', () => {
  it('should call onChange when the title is updated', () => {
    const onChange = jest.fn()
    const { getByTestId } = render(<Request title='something' method='GET' url='foo.com' onChange={onChange} />)
    fireEvent.changeText(getByTestId('titleInput'), 'my request')
    expect(onChange).toHaveBeenLastCalledWith({
      title: 'my request',
      method: 'GET',
      url: 'foo.com'
    })
  })
  it('should use defaults in onChange if the props were not provided', () => {
    const onChange = jest.fn()
    const { getByTestId } = render(<Request onChange={onChange} />)
    fireEvent.changeText(getByTestId('titleInput'), 'hello')
    expect(onChange).toHaveBeenLastCalledWith({
      title: 'hello',
      method: 'GET', // default is GET
      url: '' // default is empty string
    })
  })
})
