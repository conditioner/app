import React, { useState, useEffect } from 'react'
import { ScrollView, Text, TextInput, StyleSheet } from 'react-native'
import ResponseOutput from './ResponseOutput'

export default function Response ({ dataPath: d, response, onChange }) {
  const styles = StyleSheet.create({
    container: {
      padding: 15
    },
    label: {
      fontWeight: '700',
      marginBottom: 10
    },
    input: {
      borderColor: '#E8E8E8',
      borderWidth: 1,
      borderRadius: 4,
      backgroundColor: '#FFFFFF',
      height: 48,
      marginBottom: 20,
      padding: 10
    }
  })

  const [dataPath, setDataPath] = useState(d || '')

  useEffect(() => {
    onChange && onChange({ dataPath })
  }, [dataPath])

  return (
    <ScrollView style={styles.container}>
      <Text style={styles.label}>Data Path</Text>
      <TextInput
        autoCapitalize='none'
        style={styles.input}
        onChangeText={setDataPath}
        value={dataPath}
        autoCompleteType='off'
        testID='dataPathInput'
      />
      <Text style={styles.label}>Response</Text>
      <ResponseOutput response={response} dataPath={dataPath} setDataPath={setDataPath} />
    </ScrollView>
  )
}
