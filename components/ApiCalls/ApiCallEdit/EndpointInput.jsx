import React, { useState, useEffect } from 'react'
import { View, Text, TextInput, StyleSheet } from 'react-native'

export default function EndpointInput ({ url: u, onChange }) {
  const styles = StyleSheet.create({
    label: {
      fontWeight: '700',
      marginBottom: 10
    },
    inputWrapper: {
      borderColor: '#E8E8E8',
      borderWidth: 1,
      borderRadius: 4,
      backgroundColor: '#FFFFFF',
      flexDirection: 'row',
      marginBottom: 20
    },
    method: {
      fontWeight: '700',
      paddingLeft: 15,
      paddingRight: 15,
      height: 48,
      lineHeight: 46,
      borderColor: '#E8E8E8',
      borderRightWidth: 1
    },
    input: {
      height: 48,
      padding: 10,
      flexGrow: 1,
      flexShrink: 1
    }
  })

  const [method] = useState('GET')
  const [url, setUrl] = useState(u || '')

  useEffect(() => {
    onChange && onChange(method, url)
  }, [method, url])

  return (
    <View>
      <Text style={styles.label}>Endpoint</Text>
      <View style={styles.inputWrapper}>
        {/* for now lets just deal with GET requests and change this later */}
        <Text style={styles.method}>GET</Text>
        <TextInput
          autoCapitalize='none'
          style={styles.input}
          onChangeText={setUrl}
          value={url}
          autoCompleteType='off'
          placeholder='Enter request URL'
          testID='endpointUrl'
        />
      </View>
    </View>
  )
}
