import React from 'react'
import { render, fireEvent } from '@testing-library/react-native'
import ResponseOutput from './ResponseOutput'

describe('<ResponseOutput />', () => {
  it('should display the output value', () => {
    const { getByTestId } = render(<ResponseOutput
      dataPath='foo.bar' response={{
        foo: {
          bar: 'baz',
          qux: 'quux',
          arr: [{ foo2: 'bar2' }, { baz2: 'qux2' }]
        },
        num: 123
      }}
                                   />)
    expect(getByTestId('responseOutput').children[0]).toBe('baz')
  })
  it('should display undefined for the output if the dataPath is not in the response', () => {
    const { getByTestId } = render(<ResponseOutput
      dataPath='foo.BAR' response={{
        foo: {
          bar: 'baz',
          qux: 'quux'
        }
      }}
                                   />)
    expect(getByTestId('responseOutput').children[0]).toBe('undefined')
  })
  it('should display object for the output if the dataPath references an object in the response', () => {
    const { getByTestId } = render(<ResponseOutput
      dataPath='foo' response={{
        foo: {
          bar: 'baz',
          qux: 'quux'
        }
      }}
                                   />)
    expect(getByTestId('responseOutput').children[0]).toBe('object')
  })
  it('should display object for the output if the dataPath references an array in the response', () => {
    const { getByTestId } = render(<ResponseOutput
      dataPath='foo' response={{
        foo: [
          'bar',
          'baz'
        ]
      }}
                                   />)
    expect(getByTestId('responseOutput').children[0]).toBe('object')
  })
  it('should update the data path when a response key/value is tapped', () => {
    const setDataPath = jest.fn()
    const { queryAllByTestId } = render(<ResponseOutput
      dataPath='foo.qux' setDataPath={setDataPath} response={{
        foo: {
          bar: 'baz',
          qux: 'quux'
        }
      }}
                                        />)

    fireEvent.press(queryAllByTestId('responseKeyValue')[0])
    expect(setDataPath).toHaveBeenLastCalledWith('foo.bar')
  })
})
