import React, { useState } from 'react'
import { SafeAreaView, useWindowDimensions } from 'react-native'
import { TabView, TabBar } from 'react-native-tab-view'
import get from 'lodash.get'
import { sendRequest } from '../../../utils/api'
import { saveItem } from '../../../utils/storage'
import Request from './Request'
import Response from './Response'
import ActionButtons from '../../ActionButtons'

export default function ApiCallEdit ({ route }) {
  // we can either be updating or creating a new API call
  const item = get(route, 'params.item', {
    id: Date.now()
  })

  const [apiCall, setApiCall] = useState(item)
  const [response, setResponse] = useState(null)

  // request/response tabs state
  const layout = useWindowDimensions()
  const [index, setIndex] = React.useState(0)
  const [routes] = React.useState([
    { key: 'first', title: 'Request' },
    { key: 'second', title: 'Response' }
  ])

  const renderScene = ({ route }) => {
    switch (route.key) {
      case 'first':
        return <Request {...apiCall} onChange={handleApiCallChange} />
      case 'second':
        return <Response {...apiCall} response={response} onChange={handleApiCallChange} />
    }
  }

  function handleApiCallChange (updates) {
    setApiCall(Object.assign({}, apiCall, updates))
  }

  async function save () {
    try {
      await saveItem('api_calls', apiCall)
    } catch (e) {
      console.log(e)
    }
  }

  async function test () {
    try {
      // move to response tab on API call test (so we can see the response)
      setIndex(1)
      const response = await sendRequest(apiCall)
      setResponse(response)
    } catch (e) {
      console.log(e)
    }
  }

  const renderTabBar = props => (
    <TabBar
      {...props}
      indicatorStyle={{ backgroundColor: '#037AFF' }}
      style={{ backgroundColor: '#FFFFFF' }}
      activeColor='#000000'
      inactiveColor='#CCCCCC'
    />
  )

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <TabView
        navigationState={{ index, routes }}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={{ width: layout.width }}
        renderTabBar={renderTabBar}
      />
      <ActionButtons onTest={test} onSave={save} />
    </SafeAreaView>
  )
}
