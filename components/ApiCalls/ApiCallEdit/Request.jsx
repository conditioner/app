import React, { useState, useEffect } from 'react'
import { View, Text, TextInput, StyleSheet } from 'react-native'
import EndpointInput from './EndpointInput'

export default function Request ({ title: t, url: u, method: m, onChange }) {
  const styles = StyleSheet.create({
    container: {
      padding: 15
    },
    label: {
      fontWeight: '700',
      marginBottom: 10
    },
    input: {
      borderColor: '#E8E8E8',
      borderWidth: 1,
      borderRadius: 4,
      backgroundColor: '#FFFFFF',
      height: 48,
      marginBottom: 20,
      padding: 10
    }
  })

  const [title, setTitle] = useState(t || '')
  const [method, setMethod] = useState(m || 'GET')
  const [url, setUrl] = useState(u || '')

  function handleEndpointChange (method, url) {
    setMethod(method)
    setUrl(url)
  }

  useEffect(() => {
    onChange && onChange({ title, method, url })
  }, [title, method, url])

  return (
    <View style={styles.container}>
      <Text style={styles.label}>Title</Text>
      <TextInput
        autoCapitalize='none'
        style={styles.input}
        onChangeText={setTitle}
        value={title}
        autoCompleteType='off'
        testID='titleInput'
      />
      <EndpointInput url={url} onChange={handleEndpointChange} />
    </View>
  )
}
