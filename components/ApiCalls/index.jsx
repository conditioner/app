import React, { useState, useEffect } from 'react'
import { View, Text, FlatList, StyleSheet } from 'react-native'
import { useFocusEffect } from '@react-navigation/native'
import { getItems } from '../../utils/storage'
import ApiCallItem from './ApiCallItem'
import NoItems from '../NoItems'
import Button from '../Button'

export default function ApiCalls ({ navigation }) {
  const styles = StyleSheet.create({
    container: {
      flex: 1
    }
  })

  const [loading, setLoading] = useState(true)
  const [apiCalls, setApiCalls] = useState([])

  useEffect(() => {
    fetchData()
  }, [])

  useFocusEffect(
    React.useCallback(() => {
      fetchData()
    }, [])
  )

  function fetchData () {
    setLoading(true)
    return getItems('api_calls')
      .then(setApiCalls)
      .catch(console.log)
      .finally(() => setLoading(false))
  }

  if (loading) {
    return (<Text>Loading...</Text>)
  }

  return (
    <View style={styles.container}>
      <Button testID='createApiCall' title='Add API call' onPress={() => navigation.navigate('ApiCallEdit')} />
      {apiCalls.length === 0
        ? <NoItems message='No API Calls' />
        : <FlatList
            data={apiCalls}
            renderItem={({ item }) => <ApiCallItem navigation={navigation} {...item} />}
            keyExtractor={item => item.id.toString()}
          />}
    </View>
  )
}
