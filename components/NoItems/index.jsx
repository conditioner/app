import React from 'react'
import { View, Image, Text, StyleSheet } from 'react-native'

export default function ({ message }) {
  const styles = StyleSheet.create({
    container: {
      flexGrow: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    image: {
      width: 100,
      height: 100
    },
    text: {
      margin: 20
    }
  })

  return (
    <View testID='noItems' style={styles.container}>
      <Image source={require('./empty.png')} style={styles.image} />
      <Text testID='noItemsMessage' style={styles.text}>{message || 'No items'}</Text>
    </View>
  )
}
