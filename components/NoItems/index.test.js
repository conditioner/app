import React from 'react'
import { render } from '@testing-library/react-native'
import NoItems from './index'

describe('<NoItems />', () => {
  it('should display a message for no items', () => {
    const { getByTestId } = render(<NoItems message='Nothing here' />)
    expect(getByTestId('noItemsMessage').children[0]).toBe('Nothing here')
  })
  it('should display a default message if none set', () => {
    const { getByTestId } = render(<NoItems />)
    expect(getByTestId('noItemsMessage').children[0]).toBe('No items')
  })
})
