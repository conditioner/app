# Blocks Docs

Blocks are the combination of conditions and outputs that make up the
functionality a user can configure.

They are the cornerstone and most complex part of this application. Think of a
block as the "program" you create within the app.

## Structure

Blocks are trees with nodes.

Nodes can have one or more branches.

Output nodes or null nodes are the leaf nodes of a tree (they terminate the
program).

```
   (if)
   /  \
 (o)  (if)
      /  \
    (o)  null
```

## Storage

We store blocks as nodes where the node has references to it's children (if any)
and a reference to its parent (if not the root node).

Each node has a `type` and depending on the `type` will determine if the node
has children (e.g. a `if` node has 2 children, a `output` has no children).

```json
[{
  "id": 1,
  "type": "if",
  "trueBranch": 2,
  "falseBranch": 3
}, {
  "id": 2,
  "type": "output",
  "value": "It was true",
  "parent": 1
}, {
  "id": 3,
  "type": "if",
  "trueBranch": 4,
  "falseBranch": null,
  "parent": 1
}, {
  "id": 4,
  "type": "output",
  "value": "It was false but the other thing was true",
  "parent": 3
}]
```

## Display

### Editing

The `BlockEdit` component will store a `node` in state. This is the node
currently being edited. Only a single node is displayed at a time to be edited.

A user can traverse through the tree by updating the `node` and update its
values/configuration.

When we change node (go to a parent or child) the current node being edited is
saved.

When save is pressed we save the current `node` being edited.

### Blocks List

The blocks list shows all the root nodes and their titles.

## Nodes

### If Node

The `if` node is made up of a `condition`, `trueBranch` and `falseBranch`.

The `condition` references a condition Id from the `conditions` collection. This
is so if the condition is updated the blocks it is used in do not need to be
updated too.

The `trueBranch` and `falseBranch` reference other nodes by their Id.

### Output Node

An `output` node has no child nodes and a `value` that is output if the node is
reached.
