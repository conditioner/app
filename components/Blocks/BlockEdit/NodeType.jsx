import React, { useState } from 'react'
import { View, StyleSheet } from 'react-native'
import get from 'lodash.get'
import Buttons from '../../Button'

export default function NodeType ({ node, onChange }) {
  const styles = StyleSheet.create({
    segmented: {
      flexDirection: 'row'
    },
    segmentedIf: {
      width: 100,
      marginRight: 0,
      borderTopRightRadius: 0,
      borderBottomRightRadius: 0
    },
    segmentedOutput: {
      width: 100,
      marginLeft: 0,
      borderTopLeftRadius: 0,
      borderBottomLeftRadius: 0
    }
  })

  const [selected, setSelected] = useState(get(node, 'type', 'output'))

  function selectType (type) {
    setSelected(type)
    const { id, parent } = node
    onChange({ id, parent, type })
  }

  return (
    <View style={styles.segmented}>
      <Buttons testID='setIfNodeType' title='If' theme={selected === 'if' ? 'primary' : 'light'} style={styles.segmentedIf} onPress={() => selectType('if')} />
      <Buttons testID='setOutputNodeType' title='Output' theme={selected === 'output' ? 'primary' : 'light'} style={styles.segmentedOutput} onPress={() => selectType('output')} />
    </View>
  )
}
