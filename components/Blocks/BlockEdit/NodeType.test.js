import React from 'react'
import { render, fireEvent } from '@testing-library/react-native'
import NodeType from './NodeType'

describe('<NodeType />', () => {
  it('should update the node type to "if" when the if button is pressed', () => {
    const onChange = jest.fn()
    const node = {
      id: 1,
      parent: 2,
      type: 'output'
    }
    const { getByTestId } = render(<NodeType node={node} onChange={onChange} />)
    fireEvent.press(getByTestId('setIfNodeType'))
    expect(onChange).toHaveBeenLastCalledWith({ id: 1, parent: 2, type: 'if' })
  })
  it('should update the node type to "output" when the output button is pressed', () => {
    const onChange = jest.fn()
    const node = {
      id: 1,
      parent: 2,
      type: 'if'
    }
    const { getByTestId } = render(<NodeType node={node} onChange={onChange} />)
    fireEvent.press(getByTestId('setOutputNodeType'))
    expect(onChange).toHaveBeenLastCalledWith({ id: 1, parent: 2, type: 'output' })
  })
})
