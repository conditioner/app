import React from 'react'
import { render, waitFor } from '@testing-library/react-native'
import Node from './Node'

jest.mock('../../../utils/storage', () => {
  return {
    // mocked for <IfNode />
    getItems: jest.fn().mockImplementation(() => Promise.resolve([]))
  }
})

describe('<Node />', () => {
  it('should display a title to edit and IfNode for root nodes', async () => {
    const node = {
      id: 1
    }
    const { queryByTestId } = render(<Node node={node} />)
    await waitFor(() => queryByTestId('ifNode'))
    expect(queryByTestId('rootNodeTitle')).toBeTruthy()
    expect(queryByTestId('outputValueInput')).toBeNull()
    expect(queryByTestId('ifNode')).toBeTruthy()
  })
  it('should display an IfNode where node.type is "if"', async () => {
    const node = {
      id: 1,
      parent: 2,
      type: 'if'
    }
    const { queryByTestId } = render(<Node node={node} />)
    await waitFor(() => queryByTestId('ifNode'))
    expect(queryByTestId('rootNodeTitle')).toBeNull()
    expect(queryByTestId('outputValueInput')).toBeNull()
    expect(queryByTestId('ifNode')).toBeTruthy()
  })
  it('should display an OutputNode where node.type is "output"', () => {
    const node = {
      id: 1,
      parent: 2,
      type: 'output'
    }
    const { queryByTestId } = render(<Node node={node} />)
    expect(queryByTestId('rootNodeTitle')).toBeNull()
    expect(queryByTestId('outputValueInput')).toBeTruthy()
    expect(queryByTestId('ifNode')).toBeNull()
  })
  it('should update the header left nav to go to the parent node for nested nodes', async () => {
    const node = {
      id: 1,
      parent: 2,
      type: 'output'
    }
    const navigation = {
      setOptions: jest.fn()
    }
    render(<Node node={node} navigation={navigation} />)
    await waitFor(() => expect(navigation.setOptions).toHaveBeenCalledTimes(1))
    expect(navigation.setOptions.mock.calls[0][0].headerLeft.name).toBe('goToParentHeaderLeft')
  })
  it('should update the header left nav to go to the parent node for nested nodes', async () => {
    const node = {
      id: 1,
      type: 'if'
    }
    const navigation = {
      setOptions: jest.fn()
    }
    render(<Node node={node} navigation={navigation} />)
    await waitFor(() => expect(navigation.setOptions).toHaveBeenCalledTimes(1))
    expect(navigation.setOptions.mock.calls[0][0].headerLeft.name).toBe('defaultHeaderLeft')
  })
})
