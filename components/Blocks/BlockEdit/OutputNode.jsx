import React, { useState, useEffect } from 'react'
import { Text, TextInput, StyleSheet } from 'react-native'
import get from 'lodash.get'

export default function OutputNode ({ node, onChange }) {
  const styles = StyleSheet.create({
    label: {
      fontWeight: '700',
      marginBottom: 10
    },
    input: {
      borderColor: '#E8E8E8',
      borderWidth: 1,
      borderRadius: 4,
      backgroundColor: '#FFFFFF',
      height: 48,
      marginBottom: 20,
      padding: 10
    }
  })

  const [value, setValue] = useState(get(node, 'value', ''))

  useEffect(() => {
    onChange && onChange({ value })
  }, [value])

  return (
    <>
      <Text style={styles.label}>Output</Text>
      <TextInput
        testID='outputValueInput'
        autoCapitalize='none'
        style={styles.input}
        onChangeText={setValue}
        value={value}
        autoCompleteType='off'
      />
    </>
  )
}
