import React from 'react'
import { render, waitFor } from '@testing-library/react-native'
import IfNode from './IfNode'
import { getItem } from '../../../utils/storage'

jest.mock('../../../utils/storage', () => {
  return {
    getItems: jest.fn().mockImplementation((collection) => {
      if (collection === 'conditions') {
        return Promise.resolve([{
          id: 1,
          apiId: 1,
          operator: 'EQUALS',
          value: 'foo'
        }, {
          id: 2,
          apiId: 2,
          operator: 'GREATER_THAN',
          value: '10'
        }])
      } else if (collection === 'api_calls') {
        return Promise.resolve([{
          id: 1,
          title: 'foo api call'
        }, {
          id: 2,
          title: 'bar'
        }])
      }
    }),
    getItem: jest.fn(),
    saveItem: jest.fn().mockImplementation(() => Promise.resolve())
  }
})

describe('<IfNode />', () => {
  it('should display a picker to select the nodes condition', async () => {
    const node = {
      id: 1,
      type: 'if'
    }
    const { getByTestId } = render(<IfNode node={node} />)
    await waitFor(() => getByTestId('ifNode'))
    expect(getByTestId('pickerModal')).toBeTruthy()
  })
  it('should display a empty true branch if not set', async () => {
    const node = {
      id: 1,
      type: 'if'
    }
    const { getByTestId } = render(<IfNode node={node} />)
    await waitFor(() => getByTestId('ifNodeTrueBranchText'))
    const branch = getByTestId('ifNodeTrueBranchText')
    expect(branch.children[0]).toBe('Output nothing')
  })
  it('should display the true branch output when set', async () => {
    getItem.mockImplementation(() => Promise.resolve({
      id: 2,
      type: 'output',
      value: 'foo bar'
    }))
    const node = {
      id: 1,
      type: 'if',
      trueBranch: 2
    }
    const { getByTestId } = render(<IfNode node={node} />)
    await waitFor(() => getByTestId('ifNodeTrueBranchText'))
    const branch = getByTestId('ifNodeTrueBranchText')
    expect(branch.children[0]).toBe('Output "foo bar"')
  })
  it('should display the true branch output default message when output has no value', async () => {
    getItem.mockImplementation(() => Promise.resolve({
      id: 2,
      type: 'output'
      // no value set
    }))
    const node = {
      id: 1,
      type: 'if',
      trueBranch: 2
    }
    const { getByTestId } = render(<IfNode node={node} />)
    await waitFor(() => getByTestId('ifNodeTrueBranchText'))
    const branch = getByTestId('ifNodeTrueBranchText')
    expect(branch.children[0]).toBe('Output nothing')
  })
  it('should display the true branch is a nested if when the branch node is type is "if"', async () => {
    getItem.mockImplementation(() => Promise.resolve({
      id: 2,
      type: 'if'
    }))
    const node = {
      id: 1,
      type: 'if',
      trueBranch: 2
    }
    const { getByTestId } = render(<IfNode node={node} />)
    await waitFor(() => getByTestId('ifNodeTrueBranchText'))
    const branch = getByTestId('ifNodeTrueBranchText')
    expect(branch.children[0]).toBe('Nested if')
  })
  test.todo('should go to the true branch node if tapped and is already set')
  test.todo('should create a new node for the true branch if tapped and not already set')
  it('should display a empty false branch if not set', async () => {
    const node = {
      id: 1,
      type: 'if'
    }
    const { getByTestId } = render(<IfNode node={node} />)
    await waitFor(() => getByTestId('ifNodeFalseBranchText'))
    const branch = getByTestId('ifNodeFalseBranchText')
    expect(branch.children[0]).toBe('Output nothing')
  })
  it('should display the false branch output when set', async () => {
    getItem.mockImplementation(() => Promise.resolve({
      id: 2,
      type: 'output',
      value: 'foo bar'
    }))
    const node = {
      id: 1,
      type: 'if',
      falseBranch: 2
    }
    const { getByTestId } = render(<IfNode node={node} />)
    await waitFor(() => getByTestId('ifNodeFalseBranchText'))
    const branch = getByTestId('ifNodeFalseBranchText')
    expect(branch.children[0]).toBe('Output "foo bar"')
  })
  it('should display the false branch output default message when output has no value', async () => {
    getItem.mockImplementation(() => Promise.resolve({
      id: 2,
      type: 'output'
      // no value set
    }))
    const node = {
      id: 1,
      type: 'if',
      falseBranch: 2
    }
    const { getByTestId } = render(<IfNode node={node} />)
    await waitFor(() => getByTestId('ifNodeFalseBranchText'))
    const branch = getByTestId('ifNodeFalseBranchText')
    expect(branch.children[0]).toBe('Output nothing')
  })
  it('should display the false branch is a nested if when the branch node is type is "if"', async () => {
    getItem.mockImplementation(() => Promise.resolve({
      id: 2,
      type: 'if'
    }))
    const node = {
      id: 1,
      type: 'if',
      falseBranch: 2
    }
    const { getByTestId } = render(<IfNode node={node} />)
    await waitFor(() => getByTestId('ifNodeFalseBranchText'))
    const branch = getByTestId('ifNodeFalseBranchText')
    expect(branch.children[0]).toBe('Nested if')
  })
  test.todo('should go to the false branch node if tapped and is already set')
  test.todo('should create a new node for the false branch if tapped and not already set')
})
