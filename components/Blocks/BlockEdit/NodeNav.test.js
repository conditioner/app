import React from 'react'
import { render, waitFor, fireEvent } from '@testing-library/react-native'
import NodeNav from './NodeNav'
import { getItem } from '../../../utils/storage'

jest.mock('../../../utils/storage', () => {
  return {
    getItem: jest.fn()
  }
})

describe('<NodeNav />', () => {
  it('should navigate to the parent node if tapped', async () => {
    getItem.mockImplementation(() => Promise.resolve({
      id: 2,
      type: 'if'
    }))
    const setNode = jest.fn()
    const node = {
      id: 1,
      parent: 2,
      type: 'output'
    }
    const { getByTestId } = render(<NodeNav node={node} setNode={setNode} />)
    fireEvent.press(getByTestId('navToParentNode'))
    await waitFor(() => expect(setNode).toHaveBeenCalledTimes(1))
    expect(setNode).toHaveBeenLastCalledWith({ id: 2, type: 'if' })
  })
})
