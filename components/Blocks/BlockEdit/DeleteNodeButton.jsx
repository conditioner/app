import React from 'react'
import Button from '../../Button'
import { getChildren } from '../../../utils/block'
import { deleteItems, getItem, saveItem } from '../../../utils/storage'

export default function NodeNav ({ node, callback }) {
  function deleteNode () {
    getChildren(node)
      .then(childIds => deleteItems('nodes', [node.id].concat(childIds)))
      .then(() => node.parent ? getItem('nodes', node.parent) : null)
      .then(parent => {
        if (parent) {
          const { trueBranch, falseBranch } = parent
          if (trueBranch === node.id) {
            delete parent.trueBranch
          }
          if (falseBranch === node.id) {
            delete parent.falseBranch
          }
          return saveItem('nodes', parent)
        }
      })
      .then(() => callback && callback())
  }
  return (<Button testID='deleteNode' theme='danger' title='Delete' onPress={deleteNode} style={{ marginLeft: 'auto', marginRight: 0 }} />)
}
