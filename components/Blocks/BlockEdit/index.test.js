import React from 'react'
import { render, waitFor, fireEvent } from '@testing-library/react-native'
import BlockEdit from './index'
import { getItem, saveItem } from '../../../utils/storage'

jest.mock('../../../utils/storage', () => {
  return {
    getItem: jest.fn().mockImplementation(() => Promise.resolve({ id: 1, type: 'if' })),
    getItems: jest.fn().mockImplementation(() => Promise.resolve([])),
    saveItem: jest.fn()
  }
})

describe('<BlockEdit />', () => {
  it('should create a new node when creating a new block', async () => {
    const { getByTestId } = render(<BlockEdit />)
    await waitFor(() => getByTestId('blockEdit'))
    expect(getItem).toHaveBeenCalledTimes(0)
  })
  it('should fetch the node for the root block when editing an existing block', async () => {
    const route = { params: { id: 1 } }
    const { getByTestId } = render(<BlockEdit route={route} />)
    await waitFor(() => getByTestId('blockEdit'))
    expect(getItem).toHaveBeenCalledTimes(1)
    expect(getItem).toHaveBeenLastCalledWith('nodes', 1)
  })
  it('should save the node when save is tapped', async () => {
    const route = { params: { id: 1 } }
    const { getByTestId } = render(<BlockEdit route={route} />)
    await waitFor(() => getByTestId('blockEdit'))
    fireEvent.press(getByTestId('saveButton'))
    expect(saveItem).toHaveBeenCalledTimes(1)
    expect(saveItem).toHaveBeenLastCalledWith('nodes', { id: 1, type: 'if', title: '' })
  })
})
