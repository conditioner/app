import React, { useState, useEffect } from 'react'
import { SafeAreaView, View, Text, StyleSheet } from 'react-native'
import get from 'lodash.get'
import ActionButtons from '../../ActionButtons'
import Node from './Node'
import { getItem, saveItem } from '../../../utils/storage'
import { getRootNode, execute } from '../../../utils/block'

export default function BlockEdit ({ navigation, route }) {
  const styles = StyleSheet.create({
    container: {
      flexGrow: 1,
      padding: 15
    }
  })

  const [loading, setLoading] = useState(true)
  const [node, setNode] = useState(null)

  useEffect(() => {
    setLoading(true)
    const nodeId = get(route, 'params.id')

    if (nodeId) {
      // fetch existing block (editing)
      getItem('nodes', nodeId)
        .then(setNode)
        .catch(console.log)
        .finally(() => setLoading(false))
    } else {
      // create new block (creating)
      setNode({
        id: Date.now(),
        type: 'if'
      })
      setLoading(false)
    }
  }, [])

  async function save () {
    try {
      await saveItem('nodes', node)
    } catch (e) {
      console.log(e)
    }
  }

  async function test () {
    // find the root node and execute the block
    const rootNode = await getRootNode(node)
    const result = await execute(rootNode)
    console.log('result: ', result)
  }

  function updateNode (updates) {
    setNode(Object.assign({}, node, updates))
  }

  if (loading) {
    return (<Text>Loading...</Text>)
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <View testID='blockEdit' style={styles.container}>
        <Node node={node} onChange={updateNode} setNode={setNode} navigation={navigation} />
      </View>
      <ActionButtons onTest={test} onSave={save} />
    </SafeAreaView>
  )
}
