import React from 'react'
import { render, fireEvent } from '@testing-library/react-native'
import OutputNode from './OutputNode'

describe('<OutputNode />', () => {
  it('should display a input for the output value', () => {
    const node = {
      id: 1,
      type: 'output',
      value: 'foo bar'
    }
    const { getByTestId } = render(<OutputNode node={node} />)
    const input = getByTestId('outputValueInput')
    expect(input.props.value).toBe('foo bar')
  })
  it('should update the output value when the input value changes', () => {
    const onChange = jest.fn()
    const node = {
      id: 1,
      type: 'output',
      value: 'foo bar'
    }
    const { getByTestId } = render(<OutputNode node={node} onChange={onChange} />)
    fireEvent.changeText(getByTestId('outputValueInput'), 'foo bar baz')
    expect(onChange).toHaveBeenLastCalledWith({ value: 'foo bar baz' })
  })
})
