import React from 'react'
import { Button } from 'react-native'
import { getItem } from '../../../utils/storage'

export default function NodeNav ({ node, setNode }) {
  function gotoParent () {
    getItem('nodes', node.parent)
      .then(setNode)
  }
  return (<Button testID='navToParentNode' title='Parent Node' onPress={gotoParent} />)
}
