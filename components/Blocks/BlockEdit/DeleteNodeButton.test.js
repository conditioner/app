import React from 'react'
import { render, fireEvent, waitFor } from '@testing-library/react-native'
import DeleteNodeButton from './DeleteNodeButton'
import { getChildren } from '../../../utils/block'
import { deleteItems, getItem, saveItem } from '../../../utils/storage'

jest.mock('../../../utils/block', () => {
  return {
    getChildren: jest.fn()
  }
})
jest.mock('../../../utils/storage', () => {
  return {
    deleteItems: jest.fn(),
    getItem: jest.fn(),
    saveItem: jest.fn()
  }
})

describe('<DeleteNodeButton />', () => {
  it('should delete a node and all its children', async () => {
    deleteItems.mockImplementation(() => Promise.resolve())
    getChildren.mockResolvedValue([2, 3, 4])
    const callback = jest.fn()
    const node = {
      id: 1,
      type: 'if',
      trueBranch: 2,
      falseBranch: 3
    }
    const { getByTestId } = render(<DeleteNodeButton node={node} callback={callback} />)
    fireEvent.press(getByTestId('deleteNode'))
    await waitFor(() => expect(callback).toHaveBeenCalledTimes(1))
    expect(deleteItems).toHaveBeenLastCalledWith('nodes', [1, 2, 3, 4])
  })
  it('should update the parent node trueBranch to no longer reference the deleted node', async () => {
    deleteItems.mockImplementation(() => Promise.resolve())
    saveItem.mockImplementation(() => Promise.resolve())
    getItem.mockImplementation(() => Promise.resolve({
      id: 1,
      type: 'if',
      trueBranch: 2,
      falseBranch: 3
    }))
    getChildren.mockResolvedValue([])
    const callback = jest.fn()
    const node = {
      id: 2,
      type: 'output',
      parent: 1
    }
    const { getByTestId } = render(<DeleteNodeButton node={node} callback={callback} />)
    fireEvent.press(getByTestId('deleteNode'))
    await waitFor(() => expect(callback).toHaveBeenCalledTimes(1))
    expect(deleteItems).toHaveBeenLastCalledWith('nodes', [2])
    expect(saveItem).toHaveBeenLastCalledWith('nodes', {
      id: 1,
      type: 'if',
      falseBranch: 3
    })
  })
  it('should update the parent node falseBranch to no longer reference the deleted node', async () => {
    deleteItems.mockImplementation(() => Promise.resolve())
    saveItem.mockImplementation(() => Promise.resolve())
    getItem.mockImplementation(() => Promise.resolve({
      id: 1,
      type: 'if',
      trueBranch: 2,
      falseBranch: 3
    }))
    getChildren.mockResolvedValue([])
    const callback = jest.fn()
    const node = {
      id: 3,
      type: 'output',
      parent: 1
    }
    const { getByTestId } = render(<DeleteNodeButton node={node} callback={callback} />)
    fireEvent.press(getByTestId('deleteNode'))
    await waitFor(() => expect(callback).toHaveBeenCalledTimes(1))
    expect(deleteItems).toHaveBeenLastCalledWith('nodes', [3])
    expect(saveItem).toHaveBeenLastCalledWith('nodes', {
      id: 1,
      type: 'if',
      trueBranch: 2
    })
  })
})
