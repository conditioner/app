import React, { useState, useEffect } from 'react'
import { View, Text, TextInput, StyleSheet } from 'react-native'
import { HeaderBackButton } from '@react-navigation/stack'
import get from 'lodash.get'
import { getItem } from '../../../utils/storage'
import NodeType from './NodeType'
import IfNode from './IfNode'
import OutputNode from './OutputNode'
import DeleteNodeButton from './DeleteNodeButton'

export default function Node ({ node, onChange, setNode, navigation }) {
  const styles = StyleSheet.create({
    label: {
      fontWeight: '700',
      marginBottom: 10
    },
    input: {
      borderColor: '#E8E8E8',
      borderWidth: 1,
      borderRadius: 4,
      backgroundColor: '#FFFFFF',
      height: 48,
      marginBottom: 20,
      padding: 10
    },
    nodeToolbar: {
      flexDirection: 'row',
      marginBottom: 10
    }
  })

  const [title, setTitle] = useState(get(node, 'title', ''))

  useEffect(() => {
    onChange && onChange({ title })
  }, [title])

  useEffect(() => {
    if (node && navigation) {
      if (Object.prototype.hasOwnProperty.call(node, 'parent')) {
        return navigation.setOptions({
          headerLeft: goToParentHeaderLeft
        })
      }
      navigation.setOptions({
        headerLeft: defaultHeaderLeft
      })
    }
  }, [node, navigation])

  function gotoParent () {
    getItem('nodes', node.parent)
      .then(setNode)
  }

  function goToParentHeaderLeft (props) {
    return (<HeaderBackButton {...props} label='Parent' onPress={gotoParent} />)
  }

  function defaultHeaderLeft (props) {
    return <HeaderBackButton {...props} />
  }

  // root nodes show the title and are always if blocks
  if (!Object.prototype.hasOwnProperty.call(node, 'parent')) {
    return (
      <>
        <DeleteNodeButton node={node} callback={() => navigation.navigate('Blocks')} />
        <Text style={styles.label}>Title</Text>
        <TextInput
          testID='rootNodeTitle'
          autoCapitalize='none'
          style={styles.input}
          onChangeText={setTitle}
          value={title}
          autoCompleteType='off'
        />
        <IfNode node={node} onChange={onChange} setNode={setNode} />
      </>
    )
  }

  if (node.type === 'if') {
    return (
      <>
        <View style={styles.nodeToolbar}>
          <NodeType node={node} onChange={setNode} />
          <DeleteNodeButton node={node} callback={gotoParent} />
        </View>
        <IfNode node={node} onChange={onChange} setNode={setNode} />
      </>
    )
  } else {
    return (
      <>
        <View style={styles.nodeToolbar}>
          <NodeType node={node} onChange={setNode} />
          <DeleteNodeButton node={node} callback={gotoParent} />
        </View>
        <OutputNode node={node} onChange={onChange} />
      </>
    )
  }
}
