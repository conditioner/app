import React, { useState, useEffect } from 'react'
import { View, Text, Pressable, StyleSheet } from 'react-native'
import get from 'lodash.get'
import ModalPicker from '../../ModalPicker'
import { getItems, getItem, saveItem } from '../../../utils/storage'
import { conditions as CONDITIONS } from '../../../utils/condition'

export default function IfNode ({ node, onChange, setNode }) {
  const styles = StyleSheet.create({
    label: {
      fontSize: 24,
      fontWeight: '700',
      textAlign: 'center',
      marginBottom: 15
    }
  })

  const [loading, setLoading] = useState(true)
  const [condition, setCondition] = useState(get(node, 'condition', null))
  const [conditions, setConditions] = useState([])

  useEffect(() => {
    setLoading(true)
    Promise.all([getItems('conditions'), getItems('api_calls')])
      .then(data => {
        const [conditions, apiCalls] = data
        const merged = conditions.map(condition => {
          const apiCall = apiCalls.find(a => a.id === condition.apiId)
          const apiTitle = get(apiCall, 'title', 'Untitled API Call')
          return Object.assign({}, condition, { apiTitle })
        })
        setConditions(merged)
      })
      .catch(console.log)
      .finally(() => setLoading(false))
  }, [])

  useEffect(() => {
    onChange && onChange({ condition })
  }, [condition])

  useEffect(() => {
    setCondition(get(node, 'condition', null))
  }, [node])

  function getConditionTitle (condition) {
    return `${condition.apiTitle} ${CONDITIONS[condition.operator].symbol} ${condition.value}`
  }

  async function gotoTrueBranch () {
    if (node.trueBranch) {
      const gotoNode = await getItem('nodes', node.trueBranch)
      await saveItem('nodes', node)
      setNode && setNode(gotoNode)
    } else {
      // create a new node, set the Id for that node in the current node then go
      // to edit the new node
      const newNode = {
        id: Date.now(),
        parent: node.id,
        type: 'output'
      }
      await saveItem('nodes', Object.assign({}, node, { trueBranch: newNode.id }))
      await saveItem('nodes', newNode)
      setNode && setNode(newNode)
    }
  }

  async function gotoFalseBranch () {
    if (node.falseBranch) {
      const gotoNode = await getItem('nodes', node.falseBranch)
      await saveItem('nodes', node)
      setNode && setNode(gotoNode)
    } else {
      // create a new node, set the Id for that node in the current node then go
      // to edit the new node
      const newNode = {
        id: Date.now(),
        parent: node.id,
        type: 'output'
      }
      await saveItem('nodes', Object.assign({}, node, { falseBranch: newNode.id }))
      await saveItem('nodes', newNode)
      setNode && setNode(newNode)
    }
  }

  if (loading) {
    return (<Text>Loading...</Text>)
  }

  return (
    <View testID='ifNode'>
      <Text style={styles.label}>if</Text>
      <ModalPicker
        testID='ifNodeConditionPicker'
        label='Condition'
        options={conditions.map(c => Object.assign({ title: getConditionTitle(c) }, c))}
        selectedValue={condition}
        onChange={setCondition}
      />
      <Text style={styles.label}>then</Text>
      <IfBranch testID='ifNodeTrueBranch' target={node.trueBranch} onPress={gotoTrueBranch} />
      <Text style={styles.label}>else</Text>
      <IfBranch testID='ifNodeFalseBranch' target={node.falseBranch} onPress={gotoFalseBranch} />
    </View>
  )
}

const IfBranch = ({ testID, target, onPress }) => {
  const styles = StyleSheet.create({
    input: {
      backgroundColor: '#E1E1E1',
      height: 38,
      borderRadius: 10,
      marginBottom: 10
    },
    text: {
      fontSize: 20,
      padding: 8,
      textAlign: 'center',
      color: '#333333'
    }
  })

  const [loading, setLoading] = useState(true)
  const [text, setText] = useState('')

  useEffect(() => {
    setLoading(true)
    if (target) {
      getItem('nodes', target)
        .then(node => {
          if (node.type === 'if') {
            setText('Nested if')
          } else {
            if (node.value) {
              setText(`Output "${node.value}"`)
            } else {
              setText('Output nothing')
            }
          }
        })
        .finally(() => setLoading(false))
    } else {
      setText('Output nothing')
      setLoading(false)
    }
  }, [target])

  return (
    <Pressable testID={testID} style={styles.input} onPress={onPress}>
      {loading ? <Text style={styles.text}>...</Text> : <Text testID={`${testID}Text`} style={styles.text}>{text}</Text>}
    </Pressable>
  )
}
