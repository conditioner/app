import React, { useState, useEffect } from 'react'
import { View, Text, FlatList, StyleSheet } from 'react-native'
import { useFocusEffect } from '@react-navigation/native'
import { getItems } from '../../utils/storage'
import BlockItem from './BlockItem'
import NoItems from '../NoItems'
import Button from '../Button'

export default function Blocks ({ navigation }) {
  const styles = StyleSheet.create({
    container: {
      flex: 1
    }
  })

  const [loading, setLoading] = useState(true)
  const [blocks, setBlocks] = useState([])

  useEffect(() => {
    fetchData()
  }, [])

  useFocusEffect(
    React.useCallback(() => {
      fetchData()
    }, [])
  )

  async function fetchData () {
    setLoading(true)
    return getItems('nodes')
      .then(nodes => nodes.filter(node => !Object.prototype.hasOwnProperty.call(node, 'parent')))
      .then(setBlocks)
      .catch(console.log)
      .finally(() => setLoading(false))
  }

  if (loading) {
    return (<Text>Loading...</Text>)
  }

  return (
    <View style={styles.container}>
      <Button testID='createBlock' title='Add Block' onPress={() => navigation.navigate('BlockEdit')} />
      {blocks.length === 0
        ? <NoItems message='No Blocks' />
        : <FlatList
            data={blocks}
            renderItem={({ item }) => <BlockItem navigation={navigation} {...item} />}
            keyExtractor={item => item.id.toString()}
          />}
    </View>
  )
}
