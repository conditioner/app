import React from 'react'
import { render, waitFor, fireEvent } from '@testing-library/react-native'
import Blocks from './index'
import { getItems } from '../../utils/storage'

jest.mock('@react-navigation/native')
jest.mock('../../utils/storage', () => {
  return {
    getItems: jest.fn()
  }
})

describe('<Blocks />', () => {
  it('should fetch all blocks to display', async () => {
    getItems.mockImplementation(() => Promise.resolve([{ id: 1, title: 'foo' }, { id: 2, title: 'bar' }]))

    const { queryAllByTestId, queryByTestId } = render(<Blocks />)
    await waitFor(() => queryAllByTestId('blockListItem'))

    const noItems = queryByTestId('noItems')
    const items = queryAllByTestId('blockListItem')

    expect(noItems).toBeNull()
    expect(items.length).toEqual(2)
  })
  it('should show no items message if no blocks are saved', async () => {
    getItems.mockImplementation(() => Promise.resolve([]))

    const { getByTestId, queryAllByTestId, queryByTestId } = render(<Blocks />)
    await waitFor(() => getByTestId('noItems'))

    const noItems = queryByTestId('noItems')
    const items = queryAllByTestId('blockListItem')

    expect(items.length).toEqual(0)
    expect(noItems).toBeTruthy()
  })
  it('should navigate to BlockEdit when an item is tapped', async () => {
    const navigation = {
      navigate: jest.fn()
    }
    const { getByTestId } = render(<Blocks navigation={navigation} />)
    await waitFor(() => getByTestId('createBlock'))

    fireEvent.press(getByTestId('createBlock'))
    expect(navigation.navigate).toHaveBeenLastCalledWith('BlockEdit')
  })
})
