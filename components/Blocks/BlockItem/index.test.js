import React from 'react'
import { render, fireEvent } from '@testing-library/react-native'
import BlockItem from './index'

describe('<BlockItem />', () => {
  it('should display the blocks title', () => {
    const { queryByTestId } = render(<BlockItem title='Test Block' />)
    const title = queryByTestId('itemTitle')
    expect(title.children[0]).toBe('Test Block')
  })
  it('should display the a default title if the block has none', () => {
    const { queryByTestId } = render(<BlockItem />)
    const title = queryByTestId('itemTitle')
    expect(title.children[0]).toBe('Untitled Block')
  })
  it('should display go to BlockEdit when tapped', () => {
    const navigation = {
      navigate: jest.fn()
    }
    const { getByTestId } = render(<BlockItem id={123} title='Test Block' navigation={navigation} />)
    fireEvent.press(getByTestId('blockListItem'))
    expect(navigation.navigate).toHaveBeenLastCalledWith('BlockEdit', { id: 123 })
  })
  test.todo('should run the block on long press')
})
