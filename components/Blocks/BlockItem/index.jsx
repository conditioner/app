import React, { useState, useEffect } from 'react'
import { Pressable, Text, View, StyleSheet, Platform } from 'react-native'
import { getItem } from '../../../utils/storage'
import { execute } from '../../../utils/block'

export default function BlockItem ({ id, title, navigation }) {
  const styles = StyleSheet.create({
    container: {
      margin: 10,
      marginTop: 0,
      padding: 10,
      borderRadius: 4,
      backgroundColor: '#FFF',
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.20,
      shadowRadius: 1.41,
      elevation: 2
    },
    title: {
      fontSize: 18,
      marginBottom: 5
    },
    output: {
      backgroundColor: '#EAEAEA',
      padding: 10,
      borderRadius: 4
    },
    outputText: {
      fontFamily: Platform.OS === 'ios' ? 'Courier' : 'monospace'
    }
  })

  const [running, setRunning] = useState(true)
  const [output, setOutput] = useState('')

  useEffect(() => {
    runBlock()
  }, [])

  function getTitle () {
    if (title && title.trim() !== '') {
      return title.trim()
    } else {
      return 'Untitled Block'
    }
  }

  async function runBlock () {
    setRunning(true)
    const block = await getItem('nodes', id)
    const result = await execute(block)
    setOutput(result)
    setRunning(false)
  }

  return (
    <Pressable
      testID='blockListItem'
      style={styles.container}
      onPress={() => navigation.navigate('BlockEdit', { id })}
      onLongPress={runBlock}
    >
      <Text testID='itemTitle' style={styles.title} numberOfLines={1}>{getTitle()}</Text>
      <View style={styles.output} numberOfLines={1}>
        <Text style={styles.outputText}>{running ? 'loading...' : output}</Text>
      </View>
    </Pressable>
  )
}
