import React from 'react'
import { render, fireEvent } from '@testing-library/react-native'
import Button from './index'

const blue = '#007AFF'
const lightBlue = '#88BEFF'
const white = '#FFFFFF'
const offWhite = '#FCFCFC'
const red = '#DC3545'
const lightRed = '#E77781'

describe('<Button />', () => {
  it('should display the title passed', () => {
    const { getByTestId } = render(<Button testID='button' title='Test Title' />)
    expect(getByTestId('buttonText').children[0]).toBe('Test Title')
  })
  it('should display "Submit" if no title passed', () => {
    const { getByTestId } = render(<Button testID='button' title='' />)
    expect(getByTestId('buttonText').children[0]).toBe('Submit')
  })
  it('should call onPress when tapped', () => {
    const onPress = jest.fn()
    const { getByTestId } = render(<Button testID='button' onPress={onPress} />)
    fireEvent.press(getByTestId('button'))
    expect(onPress).toHaveBeenCalledTimes(1)
  })
  it('should be blue with white text when the theme is "primary"', () => {
    const { getByTestId } = render(<Button testID='button' />)
    expect(getByTestId('button').props.style.borderColor).toBe(blue)
    expect(getByTestId('button').props.style.backgroundColor).toBe(blue)
    expect(getByTestId('buttonText').props.style.color).toBe(white)
  })
  it('should be white with blue text and borders when the theme is "light"', () => {
    const { getByTestId } = render(<Button testID='button' theme='light' />)
    expect(getByTestId('button').props.style.borderColor).toBe(blue)
    expect(getByTestId('button').props.style.backgroundColor).toBe(white)
    expect(getByTestId('buttonText').props.style.color).toBe(blue)
  })
  it('should be red with white text when the theme is "danger"', () => {
    const { getByTestId } = render(<Button testID='button' theme='danger' />)
    expect(getByTestId('button').props.style.borderColor).toBe(red)
    expect(getByTestId('button').props.style.backgroundColor).toBe(red)
    expect(getByTestId('buttonText').props.style.color).toBe(white)
  })
  it('should be light blue with white text when the theme is "primary" and disabled', () => {
    const { getByTestId } = render(<Button testID='button' disabled />)
    expect(getByTestId('button').props.style.borderColor).toBe(lightBlue)
    expect(getByTestId('button').props.style.backgroundColor).toBe(lightBlue)
    expect(getByTestId('buttonText').props.style.color).toBe(white)
  })
  it('should be white with light blue text and borders when the theme is "default" and disabled', () => {
    const { getByTestId } = render(<Button testID='button' theme='light' disabled />)
    expect(getByTestId('button').props.style.borderColor).toBe(lightBlue)
    expect(getByTestId('button').props.style.backgroundColor).toBe(offWhite)
    expect(getByTestId('buttonText').props.style.color).toBe(lightBlue)
  })
  it('should be light red with white text when the theme is "danger" and disabled', () => {
    const { getByTestId } = render(<Button testID='button' theme='danger' disabled />)
    expect(getByTestId('button').props.style.borderColor).toBe(lightRed)
    expect(getByTestId('button').props.style.backgroundColor).toBe(lightRed)
    expect(getByTestId('buttonText').props.style.color).toBe(white)
  })
})
