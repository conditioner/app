import React from 'react'
import { Pressable, Text, StyleSheet } from 'react-native'

export default function ActionButtons ({ children, title, disabled, onPress, testID, theme = 'primary', style = {} }) {
  const styles = StyleSheet.create({
    button: Object.assign({
      backgroundColor: getBackgroundColor(theme, disabled),
      borderColor: getBorderColor(theme, disabled),
      borderWidth: 1,
      borderRadius: 6,
      padding: 9,
      paddingLeft: 22,
      paddingRight: 22,
      margin: 10,
      marginLeft: 'auto',
      marginRight: 'auto'
    }, style),
    buttonText: {
      color: getTextColor(theme, disabled),
      fontSize: 17,
      textAlign: 'center'
    }
  })

  function getBorderColor () {
    if (theme === 'primary') {
      if (disabled) {
        return '#88BEFF'
      }
      return '#007AFF'
    }
    if (theme === 'danger') {
      if (disabled) {
        return '#E77781'
      }
      return '#DC3545'
    }

    return disabled ? '#88BEFF' : '#007AFF'
  }

  function getTextColor () {
    if (theme === 'primary' || theme === 'danger') {
      return '#FFFFFF'
    }

    return disabled ? '#88BEFF' : '#007AFF'
  }

  function getBackgroundColor () {
    if (theme === 'primary') {
      if (disabled) {
        return '#88BEFF'
      }
      return '#007AFF'
    }
    if (theme === 'danger') {
      if (disabled) {
        return '#E77781'
      }
      return '#DC3545'
    }

    return disabled ? '#FCFCFC' : '#FFFFFF'
  }

  return (
    <Pressable style={styles.button} onPress={onPress} testID={testID} android_ripple={{ radius: 50 }} disabled={disabled}>
      {children || <Text testID={`${testID}Text`} style={styles.buttonText}>{title || 'Submit'}</Text>}
    </Pressable>
  )
}
