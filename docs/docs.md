# Logic Concept

The conditioner apps goal is to provide a simple set of building blocks to
configure useful outputs based in conditions.

At its core this is not to dissimilar from any programming language. To allow
for a simplified experience the building blocks available are limited. This
lets you build fairly complex logic in a no-code like experience.

## The Core Building Blocks

* API Calls
* Conditions
* Blocks
* Outputs

![Building blocks example](./logic-building-blocks.png)

### API Calls

You configure an API call to fetch data you're interested in for your
conditions.

The request you configure will typically include the request method (e.g. `GET`,
`POST`, `PUT` etc.), a URL (e.g. `https://example.com/api/foo`) and headers
(e.g. `Authorization`).

For the response you configure what data to extract. By setting a path to where
in the response bdy to look you set your API calls output.

### If Conditions

Conditions are made up of 3 parts. A input (e.g. an API call output), a
relational operator (e.g. greater than, equal to, less than) and a value to
compare to.

### Blocks

A block is where you configure logic based on a condition. Each block will
accept:

* A condition
* What to do if that condition evaluates to true
* What to do if the condition evaluates to false

The true and false branches of a block can be either an output or another nested
block.

Blocks and nested blocks will result in a tree structure of logic. A reference
to a block cannot be repeated in this tree structure (no maps/loops).

### Output

An output terminates logic and displays something.

## Logical Operators

### AND

![AND Logical Operator](./logic-AND.png)

### OR

![OR Logical Operator](./logic-OR.png)

## Limitations

While the functionality the app gives is somewhat akin to a programming language
its fair to say it is (intentionally) nowhere near as fully featured as you
would expect a programming language to be.

Some of the notable omissions are:

* No loops
* No variables
* No functions
* No actions (e.g. writing to disk or calling APIs based on logic)

It is however possible some of these may be added in the future as new features.
