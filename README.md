[![pipeline status](https://gitlab.com/conditioner/app/badges/main/pipeline.svg)](https://gitlab.com/conditioner/app/-/commits/main)
[![coverage report](https://gitlab.com/conditioner/app/badges/main/coverage.svg)](https://gitlab.com/conditioner/app/-/commits/main)

# :bath: Conditioner

An app for iOS and Android to create useful output from API endpoints and
conditions you configure.

## :computer: Developing

Clone the repo and start with the steps below to get going.

### :runner: Run using Expo

[Install expo][1] and the optional recommended tools such as expo go to run on
an Apple or Android device.

From the project root install the projects dependencies with `npm i` and the
start with `npm start`.

If you need more help try the [expo forums][2] for troubleshooting.

### :atom: Develop using React Native

This project uses the managed expo workflow. You'll be able to take advantage of
most of React Natives features. However there are [some limitations][3].

### :clown: Test with Jest

We have tests written with Jest. The [expo docs on testing][4] give a good
general introduction and the [Jest docs][5] go further into what's possible.

Note as per the expo docs we add Jest via `jest-expo` which adds Jest `25.x`
instead of the latests version.

We also [react-native-testing-library][6] to add testing utility methods.

[1]: https://docs.expo.io/get-started/installation/
[2]: https://forums.expo.io/
[3]: https://docs.expo.io/introduction/why-not-expo/
[4]: https://docs.expo.io/guides/testing-with-jest/
[5]: https://jestjs.io/docs/25.x/getting-started
[6]: https://callstack.github.io/react-native-testing-library/docs/getting-started
