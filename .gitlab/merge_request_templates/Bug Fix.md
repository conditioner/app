# :bug: [TITLE]

## :book: Description

[Brief description of the bug fix - Cause of issue & fix]

### :construction: Remaining Work (optional)

[Any notes or related or other remaining work to be done]

### :notebook: Notes (optional)

[Any additional notes not already mentioned]

## :white_check_mark: Testing

[Any manual test steps not covered by CI pipeline]

## :microscope: Reviewing

1. [ ] Check latest pipeline passes
1. [ ] Appropriate documentation added
1. [ ] Test coverage is acceptable
1. [ ] Regression tests added
