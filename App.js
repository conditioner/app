import React from 'react'
import { NavigationContainer, getFocusedRouteNameFromRoute } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import MainTabsNavigation from './components/MainTabsNavigation'
import ApiCallEdit from './components/ApiCalls/ApiCallEdit'
import ConditionEdit from './components/Conditions/ConditionEdit'
import BlockEdit from './components/Blocks/BlockEdit'

const Stack = createStackNavigator()

export default function App () {
  function getHeaderTitle (route) {
    // If the focused route is not found, we need to assume it's the initial screen
    // This can happen during if there hasn't been any navigation inside the screen
    // In our case, it's "StoreSomething" as that's the first screen inside the navigator
    const routeName = getFocusedRouteNameFromRoute(route) ?? 'Conditions'

    switch (routeName) {
      case 'Conditions':
        return 'Conditions'
      case 'ApiCalls':
        return 'API Calls'
      case 'Blocks':
        return 'Blocks'
    }
  }

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='MainTabsNavigation'>
        <Stack.Screen
          name='MainTabsNavigation'
          component={MainTabsNavigation}
          options={({ route }) => ({
            headerTitle: getHeaderTitle(route)
          })}
        />
        <Stack.Screen
          name='ApiCallEdit'
          component={ApiCallEdit}
          options={{
            title: 'API Call Editor',
            headerStyle: {
              shadowColor: 'transparent',
              elevation: 0
            }
          }}
        />
        <Stack.Screen
          name='ConditionEdit'
          component={ConditionEdit}
          options={{ title: 'Condition Editor' }}
        />
        <Stack.Screen
          name='BlockEdit'
          component={BlockEdit}
          options={{ title: 'Block Editor' }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  )
}
